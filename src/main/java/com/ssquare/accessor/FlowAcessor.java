package com.ssquare.accessor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;

import com.ssquare.common.FlowCommon;
import com.ssquare.facade.accessor.ServiceAccessor;
import com.ssquare.facade.common.handler.ssquare.Flow;
import com.ssquare.util.RootDataUtil;
import com.ssquare.util.WidgetsUtil;

public class FlowAcessor {

	/**
	 * Load SSQ Node from config
	 * 
	 * @param accessor
	 * @throws Exception
	 */
	public void loadSSQNode(ServiceAccessor accessor) throws Exception {

		// To get logger
		Log logger = accessor.common.logging.getLogger(this);

		// Stamp log to start
		logger.debug("loadCommonNode start");

		// To do something in try block
		try {

			// To get data part in request
			Map<String, Object> data = accessor.context.request.data;

			// To get some param in data
			String search = (String) data.get("search");

			// To call your common if you want
			FlowCommon flowCom = new FlowCommon();
			List<Map<String, Object>> widgets = flowCom.loadSSQNode(search);

			// Prepare the result to return
			Map<String, Object> result = accessor.context.response.data;
			result.put("widgets", widgets);

		} catch (Exception e) {
			logger.error(e);
			throw e;
		}

		logger.debug("loadCommonNode end");

	}

	/**
	 * Commit flow to SSQ Database
	 * 
	 * @param accessor
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked" })
	public void commitFlow(ServiceAccessor accessor) throws Exception {

		// To get logger
		Log logger = accessor.common.logging.getLogger(this);

		// Stamp log to start
		logger.debug("commitFlow start");

		// To do something in try block
		try {

			// To get data part in request
			Map<String, Object> data = accessor.context.request.data;

			// To get some param in data
			// String username = (String) data.get("username");
			String graphModel = (String) data.get("graphModel");
			Map<String, Object> propModel = (Map<String, Object>) data.get("propModel");
			String canvasId = (String) data.get("canvasId");
			String username = (String) data.get("username");
			long serviceId = accessor.common.number.toLong(canvasId);

			// Promote model
			// $promoteModel$" from Redis, promoteModel from frontend
			List<Map<String, Object>> promoteModel = null;
			if (propModel != null) {
				promoteModel = (List<Map<String, Object>>) propModel.get("$promoteModel$");
				if (promoteModel != null) {
					propModel.remove("$promoteModel$");
				} else {
					promoteModel = (List<Map<String, Object>>) propModel.get("promoteModel");
					propModel.remove("promoteModel");
				}
			}

			// System.out.println("graphModel=" + graphModel);

			// To get flow
			Flow flow = accessor.common.ssquare.getFlow(serviceId, graphModel);

			// Save property of each node
			FlowCommon flowCom = new FlowCommon();
			flowCom.setNodeProperties(flow, propModel);

			// Set promoteModel
			flowCom.savePromoteModel(flow, promoteModel);

			// Save flow to database
			flow.saveScreenFlow();

			// Return data to response
			Map<String, Object> resData = accessor.context.response.data;
			resData.put("canvasId", canvasId);
			resData.put("username", username);

			// Prepare the result to return
			accessor.returnSuccess();

		} catch (Exception e) {
			logger.error(e);
			throw e;
		}

		logger.debug("commitFlow end");

	}

	/**
	 * Load flow from database
	 * 
	 * @param serviceAcc
	 * @throws Exception
	 */
	public void loadFlow(ServiceAccessor accessor) throws Exception {

		// To get logger
		Log logger = accessor.common.logging.getLogger(this);

		// Stamp log to start
		logger.debug("commitFlow start");

		// To do something in try block
		try {

			// To get data part in request
			Map<String, Object> data = accessor.context.request.data;

			// To get some param in data
			String username = (String) data.get("username");
			String canvasId = (String) data.get("canvasId");
			long serviceId = accessor.common.number.toLong(canvasId);

			// To get flow
			Flow flow = accessor.common.ssquare.getFlow(serviceId);

			// Create flowCom
			FlowCommon flowCom = new FlowCommon();

			// graphModel
			String graphModel = flowCom.getGraphModel(flow);

			// propModel
			Map<String, Object> propModel = flowCom.getPropModel(flow);

			// oldPromoteModel
			List<Map<String, Object>> oldPromote = flowCom.convertToCanvasPromoteModel(flow);
			logger.debug("oldPromote = " + oldPromote);

			// $promoteModel$
			List<Map<String, Object>> promoteModel = flowCom.loadPromoteModel(flow, propModel, oldPromote);
			propModel.put("$promoteModel$", promoteModel);

			// Prepare the result to return
			Map<String, Object> resData = accessor.context.response.data;
			resData.put("graphModel", graphModel);
			resData.put("propModel", propModel);
			resData.put("canvasId", canvasId);
			resData.put("username", username);

			// Return successful
			accessor.returnSuccess();

		} catch (Exception e) {
			logger.error(e);
			throw e;
		}

		logger.debug("commitFlow end");

	}

	/**
	 * To list field name in the flow
	 * 
	 * @param accessor
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public void listFieldName(ServiceAccessor accessor) throws Exception {

		// To get logger
		Log logger = accessor.common.logging.getLogger(this);

		// Stamp log to start
		logger.debug("listFieldName start");

		// To do something in try block
		try {

			// To get data part in request
			Map<String, Object> data = accessor.context.request.data;

			// To get some param in data
			// String username = (String) data.get("username");
			Map<String, Object> propModel = (Map<String, Object>) data.get("propModel");
			String canvasId = (String) data.get("canvasId");
			String username = (String) data.get("username");
			long serviceId = accessor.common.number.toLong(canvasId);

			// Promote model
			// $promoteModel$ from Redis, promoteModel from frontend
			List<Map<String, Object>> promoteModel = null;
			if (propModel != null) {
				promoteModel = (List<Map<String, Object>>) propModel.get("$promoteModel$");
				if (promoteModel != null) {
					propModel.remove("$promoteModel$");
				} else {
					promoteModel = (List<Map<String, Object>>) propModel.get("promoteModel");
					propModel.remove("promoteModel");
				}
			}

			// To get flow
			Flow flow = accessor.common.ssquare.getFlow(serviceId);

			// Save property of each node
			FlowCommon flowCom = new FlowCommon();

			// Load field name of normal file
			List<Map<String, Object>> list = flowCom.listFieldName(flow, propModel, false, promoteModel);

			// Return data to response
			Map<String, Object> resData = accessor.context.response.data;
			resData.put("canvasId", canvasId);
			resData.put("username", username);
			resData.put("list", list);

			// Prepare the result to return
			accessor.returnSuccess();

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		logger.debug("listFieldName end");

	}

	/**
	 * To list field data list name in the flow
	 * 
	 * @param accessor
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public void listDataListName(ServiceAccessor accessor) throws Exception {

		// To get logger
		Log logger = accessor.common.logging.getLogger(this);

		// Stamp log to start
		logger.debug("listDataListName start");

		// To do something in try block
		try {

			// To get data part in request
			Map<String, Object> data = accessor.context.request.data;

			// To get some param in data
			// String username = (String) data.get("username");
			Map<String, Object> propModel = (Map<String, Object>) data.get("propModel");
			String canvasId = (String) data.get("canvasId");
			String username = (String) data.get("username");
			long serviceId = accessor.common.number.toLong(canvasId);

			// Promote model
			// $promoteModel$" from Redis, promoteModel from frontend
			List<Map<String, Object>> promoteModel = null;
			if (propModel != null) {
				promoteModel = (List<Map<String, Object>>) propModel.get("$promoteModel$");
				if (promoteModel != null) {
					propModel.remove("$promoteModel$");
				} else {
					promoteModel = (List<Map<String, Object>>) propModel.get("promoteModel");
					propModel.remove("promoteModel");
				}
			}

			// To get flow
			Flow flow = accessor.common.ssquare.getFlow(serviceId);

			// Save property of each node
			FlowCommon flowCom = new FlowCommon();
			List<Map<String, Object>> list = flowCom.listFieldName(flow, propModel, true, promoteModel);

			// Return data to response
			Map<String, Object> resData = accessor.context.response.data;
			resData.put("canvasId", canvasId);
			resData.put("username", username);
			resData.put("list", list);

			// Prepare the result to return
			accessor.returnSuccess();

		} catch (Exception e) {
			logger.error(e);
			throw e;
		}

		logger.debug("listDataListName end");

	}

	@SuppressWarnings("unchecked")
	public void listFieldNameInDataList(ServiceAccessor accessor) throws Exception {

		// To get logger
		Log logger = accessor.common.logging.getLogger(this);

		// Stamp log to start
		logger.debug("listFieldNameInDataList start");

		// To do something in try block
		try {

			// To get data part in request
			Map<String, Object> data = accessor.context.request.data;

			// To get some param in data
			// String username = (String) data.get("username");
			Map<String, Object> propModel = (Map<String, Object>) data.get("propModel");
			String canvasId = (String) data.get("canvasId");
			String username = (String) data.get("username");
			String listName = (String) data.get("listName");
			long serviceId = accessor.common.number.toLong(canvasId);

			// To get flow
			Flow flow = accessor.common.ssquare.getFlow(serviceId);

			// Save property of each node
			FlowCommon flowCom = new FlowCommon();
			List<Map<String, Object>> list = flowCom.listFieldNameInDataList(flow, propModel, listName);

			// Return data to response
			Map<String, Object> resData = accessor.context.response.data;
			resData.put("canvasId", canvasId);
			resData.put("username", username);
			resData.put("listName", listName);
			resData.put("list", list);

			// Prepare the result to return
			accessor.returnSuccess();

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		logger.debug("listFieldNameInDataList end");

	}

	@SuppressWarnings("unchecked")
	public void listPromoteField(ServiceAccessor accessor) throws Exception {

		// To get logger
		Log logger = accessor.common.logging.getLogger(this);

		// Stamp log to start
		logger.debug("listPromoteField start");

		// To do something in try block
		try {

			// To get data part in request
			Map<String, Object> data = accessor.context.request.data;

			// To get some param in data
			// String username = (String) data.get("username");
			Map<String, Object> propModel = (Map<String, Object>) data.get("propModel");
			String canvasId = (String) data.get("canvasId");
			String username = (String) data.get("username");
			long serviceId = accessor.common.number.toLong(canvasId);

			// Promote model
			// $promoteModel$ from Redis, promoteModel from frontend
			List<Map<String, Object>> promoteModel = null;
			if (propModel != null) {
				promoteModel = (List<Map<String, Object>>) propModel.get("$promoteModel$");
				if (promoteModel != null) {
					propModel.remove("$promoteModel$");
				} else {
					promoteModel = (List<Map<String, Object>>) propModel.get("promoteModel");
					propModel.remove("promoteModel");
				}
			}

			// To get flow
			Flow flow = accessor.common.ssquare.getFlow(serviceId);

			// Save property of each node
			FlowCommon flowCom = new FlowCommon();
			List<Map<String, Object>> newPromoteModel = flowCom.loadPromoteModel(flow, propModel, promoteModel);

			// Return data to response
			Map<String, Object> resData = accessor.context.response.data;
			resData.put("canvasId", canvasId);
			resData.put("username", username);
			resData.put("promoteModel", newPromoteModel);

			// Prepare the result to return
			accessor.returnSuccess();

		} catch (Exception e) {
			logger.error(e);
			throw e;
		}

		logger.debug("listPromoteField end");

	}

	/**
	 * list promote field in tree mode
	 * 
	 * @param accessor
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public void splitPromoteField(ServiceAccessor accessor) throws Exception {

		// To get logger
		Log logger = accessor.common.logging.getLogger(this);

		// Stamp log to start
		logger.debug("splitPromoteField start");

		// To do something in try block
		try {

			// To get data part in request
			Map<String, Object> data = accessor.context.request.data;

			// To get some param in data
			// String username = (String) data.get("username");
			Map<String, Object> propModel = (Map<String, Object>) data.get("propModel");
			String canvasId = (String) data.get("canvasId");
			String username = (String) data.get("username");
			// long serviceId = accessor.common.number.toLong(canvasId);

			boolean showType = false;
			if (data.get("showType") != null) {
				showType = (boolean) data.get("showType");
			}

			// Promote model
			// promoteModel from frontend, $promoteModel$ from Radis
			List<Map<String, Object>> promoteModel = null;
			if (propModel != null) {
				promoteModel = (List<Map<String, Object>>) propModel.get("$promoteModel$");
				if (promoteModel != null) {
					propModel.remove("$promoteModel$");
				} else {
					promoteModel = (List<Map<String, Object>>) propModel.get("promoteModel");
					propModel.remove("promoteModel");
				}
			}

			// Splite Promote Input Output
			List<Map<String, Object>> promoteInput = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> promoteOutput = new ArrayList<Map<String, Object>>();
			Map<String, Object> inputOutput = new HashMap<String, Object>();
			RootDataUtil.splitPromote(promoteModel, promoteInput, showType, true);
			RootDataUtil.splitPromote(promoteModel, promoteOutput, showType, false);
			inputOutput.put("promoteInput", promoteInput);
			inputOutput.put("promoteOutput", promoteOutput);

			// Return data to response
			Map<String, Object> resData = accessor.context.response.data;
			resData.put("canvasId", canvasId);
			resData.put("username", username);
			resData.put("promoteModel", inputOutput);

			// Prepare the result to return
			accessor.returnSuccess();

		} catch (Exception e) {
			logger.error(e);
			throw e;
		}

		logger.debug("splitPromoteField end");

	}

	/**
	 * To load information of the node
	 * 
	 * @param serviceAcc
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public void loadNodeInfo(ServiceAccessor accessor) throws Exception {

		// To get logger
		Log logger = accessor.common.logging.getLogger(this);

		// Stamp log to start
		logger.debug("loadNodeInfo start");

		// To do something in try block
		try {

			// To get data part in request
			Map<String, Object> data = accessor.context.request.data;

			// To get some param in data
			String canvasId = (String) data.get("canvasId");
			String username = (String) data.get("username");
			String nodeType = (String) data.get("nodeType");
			Map<String, Object> nodeProperty = (Map<String, Object>) data.get("nodeProperty");

			// To find node info
			Map<String, Object> nodeInfo = WidgetsUtil.getNodeInfo(nodeType);
			if (nodeProperty == null) {
				nodeProperty = new HashMap<String, Object>();
			}
			if(nodeInfo != null) {
				nodeProperty.put("nodeInfo", nodeInfo);
			}

			// To find panel info of this node
			Map<String, Object> panelInfo = WidgetsUtil.getPanelInfo(nodeType);

			// Return data to response
			Map<String, Object> resData = accessor.context.response.data;
			resData.put("canvasId", canvasId);
			resData.put("username", username);
			resData.put("nodeType", nodeType);
			resData.put("nodeProperty", nodeProperty);
			resData.put("panelInfo", panelInfo);

			// Prepare the result to return
			accessor.returnSuccess();

		} catch (Exception e) {
			logger.error(e);
			throw e;
		}

		logger.debug("loadNodeInfo end");

	}

}