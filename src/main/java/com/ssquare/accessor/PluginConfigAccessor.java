package com.ssquare.accessor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;

import com.ssquare.common.PluginConfigCommon;
import com.ssquare.facade.accessor.ServiceAccessor;
import com.ssquare.facade.common.handler.ssquare.Flow;

/**
 * Load plugin config from postgres database
 * 
 * @author adirak-vdi
 *
 */
public class PluginConfigAccessor {

	/**
	 * To refresh web-service properties by 2 step 1. load data from WSDL 2. merge
	 * data from current data (from front end)
	 * 
	 * @param accessor
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked" })
	public void loadWSDL(ServiceAccessor accessor) throws Exception {

		// To get logger
		Log logger = accessor.common.logging.getLogger(this);

		// Stamp log to start
		logger.debug("loadWSDL start");

		// To do something in try block
		try {

			// To get data part in request
			Map<String, Object> data = accessor.context.request.data;

			// To get some param in data
			String canvasId = (String) data.get("canvasId");
			String username = (String) data.get("username");
			long serviceId = accessor.common.number.toLong(canvasId);
			String nodeId = (String) data.get("nodeId");
			String nodeType = (String) data.get("nodeType");
			String portType = (String) data.get("portType");
			String operation = (String) data.get("operation");
			String endpointName = (String) data.get("endpointName");

			// To get property
			Map<String, Object> nodeProperty = (Map<String, Object>) data.get("nodeProperty");
			if (nodeProperty == null) {
				nodeProperty = new HashMap<String, Object>();
			}
			if (nodeProperty.get("nodeType") == null) {
				nodeProperty.put("nodeType", nodeType);
			}
			if (nodeProperty.get("nodeId") == null) {
				nodeProperty.put("nodeId", nodeId);
			}

			// To get flow
			Flow flow = accessor.common.ssquare.getFlow(serviceId);

			// Save property of each node
			PluginConfigCommon plugCom = new PluginConfigCommon();
			Map<String, Object> newNodeProps = plugCom.loadWSDL(flow, nodeProperty, endpointName, portType, operation);

			// Return data to response
			Map<String, Object> resData = accessor.context.response.data;
			resData.put("canvasId", canvasId);
			resData.put("username", username);
			resData.put("canvasId", canvasId);
			resData.put("nodeId", nodeId);
			resData.put("portType", portType);
			resData.put("operation", operation);
			resData.put("endpointName", endpointName);
			resData.put("nodeProperty", newNodeProps);
			resData.put("nodeType", newNodeProps.get("nodeType"));

			// Prepare the result to return
			accessor.returnSuccess();

		} catch (Exception e) {
			logger.error("internal error! ", e);
			throw e;
		}

		logger.debug("loadWSDL end");

	}

	/**
	 * To refresh web-service properties by 2 step 1. load data from copybook 2.
	 * merge data from current data (from front end)
	 * 
	 * @param accessor
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked" })
	public void loadCopybook(ServiceAccessor accessor) throws Exception {

		// To get logger
		Log logger = accessor.common.logging.getLogger(this);

		// Stamp log to start
		logger.debug("loadCopybook start");

		// To do something in try block
		try {

			// To get data part in request
			Map<String, Object> data = accessor.context.request.data;

			// To get some param in data
			String canvasId = (String) data.get("canvasId");
			String username = (String) data.get("username");
			long serviceId = accessor.common.number.toLong(canvasId);
			String nodeId = (String) data.get("nodeId");
			String nodeType = (String) data.get("nodeType");
			String programCallConfigName = (String) data.get("programCallConfigName");

			// To get property
			Map<String, Object> nodeProperty = (Map<String, Object>) data.get("nodeProperty");
			if (nodeProperty == null) {
				nodeProperty = new HashMap<String, Object>();
			}
			if (nodeProperty.get("nodeType") == null) {
				nodeProperty.put("nodeType", nodeType);
			}
			if (nodeProperty.get("nodeId") == null) {
				nodeProperty.put("nodeId", nodeId);
			}

			// To get flow
			Flow flow = accessor.common.ssquare.getFlow(serviceId);

			// Save property of each node
			PluginConfigCommon plugCom = new PluginConfigCommon();
			Map<String, Object> newNodeProps = plugCom.loadCopybook(flow, nodeProperty, programCallConfigName);

			// Return data to response
			Map<String, Object> resData = accessor.context.response.data;
			resData.put("canvasId", canvasId);
			resData.put("username", username);
			resData.put("canvasId", canvasId);
			resData.put("nodeId", nodeId);
			resData.put("programCallConfigName", programCallConfigName);
			resData.put("nodeProperty", newNodeProps);
			resData.put("nodeType", newNodeProps.get("nodeType"));

			// Prepare the result to return
			accessor.returnSuccess();

		} catch (Exception e) {
			logger.error(e);
			throw e;
		}

		logger.debug("loadCopybook end");

	}

	/**
	 * To refresh web-service properties by 2 step 1. load data from copybook 2.
	 * merge data from current data (from front end)
	 * 
	 * @param accessor
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked" })
	public void refreshProgramCallData(ServiceAccessor accessor) throws Exception {

		// To get logger
		Log logger = accessor.common.logging.getLogger(this);

		// Stamp log to start
		logger.debug("refreshProgramCallData start");

		// To do something in try block
		try {

			// To get data part in request
			Map<String, Object> data = accessor.context.request.data;

			// To get some param in data
			String canvasId = (String) data.get("canvasId");
			String username = (String) data.get("username");
			long serviceId = accessor.common.number.toLong(canvasId);
			String nodeId = (String) data.get("nodeId");
			String nodeType = (String) data.get("nodeType");

			// To get property
			Map<String, Object> nodeProperty = (Map<String, Object>) data.get("nodeProperty");
			if (nodeProperty == null) {
				nodeProperty = new HashMap<String, Object>();
			}
			if (nodeProperty.get("nodeType") == null) {
				nodeProperty.put("nodeType", nodeType);
			}
			if (nodeProperty.get("nodeId") == null) {
				nodeProperty.put("nodeId", nodeId);
			}

			// To get flow
			Flow flow = accessor.common.ssquare.getFlow(serviceId);

			// Save property of each node
			PluginConfigCommon plugCom = new PluginConfigCommon();
			Map<String, Object> newNodeProps = plugCom.refreshProgramCallData(flow, nodeProperty);

			// Return data to response
			Map<String, Object> resData = accessor.context.response.data;
			resData.put("canvasId", canvasId);
			resData.put("username", username);
			resData.put("canvasId", canvasId);
			resData.put("nodeId", nodeId);
			resData.put("nodeProperty", newNodeProps);
			resData.put("nodeType", newNodeProps.get("nodeType"));

			// Prepare the result to return
			accessor.returnSuccess();

		} catch (Exception e) {
			logger.error(e);
			throw e;
		}

		logger.debug("refreshProgramCallData end");

	}

	/**
	 * List field in screen
	 * 
	 * @param accessor
	 */
	public void listFieldInScreen(ServiceAccessor accessor) {

		// To get logger
		Log logger = accessor.common.logging.getLogger(this);

		// Stamp log to start
		logger.debug("listFieldInScreen start");

		// To do something in try block
		try {

			// To get data part in request
			Map<String, Object> data = accessor.context.request.data;

			// To get some param in data
			String canvasId = (String) data.get("canvasId");
			String username = (String) data.get("username");
			String screenName = (String) data.get("screenName");

			// Save property of each node
			PluginConfigCommon plugCom = new PluginConfigCommon();
			List<Map<String, Object>> fields = plugCom.listFieldInScreen(screenName);

			// Return data to response
			Map<String, Object> resData = accessor.context.response.data;
			resData.put("canvasId", canvasId);
			resData.put("username", username);
			resData.put("canvasId", canvasId);
			resData.put("fields", fields);

			// Prepare the result to return
			accessor.returnSuccess();

		} catch (Exception e) {
			logger.error(e);
			throw e;
		}

		logger.debug("listFieldInScreen end");

	}

}
