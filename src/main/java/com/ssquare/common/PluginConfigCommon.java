package com.ssquare.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;

import com.ssquare.consts.DataTypeConsts;
import com.ssquare.facade.common.Common;
import com.ssquare.facade.common.handler.ssquare.Flow;
import com.ssquare.facade.common.handler.ssquare.data.NodeCell;
import com.ssquare.property.NodeProps;
import com.ssquare.util.RootDataUtil;

import techberry.ewi.data.bean.entity.FieldBean;
import techberry.ewi.graph.editor.EWICell;
import techberry.ewi.graph.editor.PCData;
import techberry.ewi.graph.editor.WSData;

public class PluginConfigCommon extends Common {

	/**
	 * To get prop model
	 * 
	 * @param flow
	 * @return propModel
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> loadWSDL(Flow flow, Map<String, Object> nodeProperty, String endpointName, String portType, String operation) throws Exception {

		// try to load wsdl
		try {

			long flowId = flow.getFlowId();
			String nodeId = (String) nodeProperty.get("nodeId");
			String nodeType = (String) nodeProperty.get("nodeType");

			if (portType != null) {
				nodeProperty.put("portType", portType);
			}

			if (operation != null) {
				nodeProperty.put("operation", operation);
			}

			if (endpointName != null) {
				nodeProperty.put("endpointName", endpointName);
			}

			// Find ewi cell in flow
			// ****************************
			EWICell cell = new EWICell(EWICell.WEBSERVICE);

			if (cell.getWSData() == null) {
				cell.setWSData(new WSData());
			}

			// ****************************

			Map<String, Object> newProps = new HashMap<String, Object>();

			// Make data to call interface
			NodeCell nodeCell = NodeCell.create(flowId, nodeId, flow, cell, newProps);

			// Node Props
			// Load properties
			try {

				NodeProps nodeProps = FlowCommon.getNodeProp(nodeCell);
				Object outProps = nodeProps.mergePlugConfig(nodeCell, nodeProperty);

				// convert new prop
				newProps = (Map<String, Object>) outProps;

				// Add node info
				newProps.put("nodeType", nodeType);
				newProps.put("nodeId", nodeId);

				return newProps;

			} catch (Exception e) {
				Log log = common.logging.getLogger(this);
				log.info(null, e);
				log.info("portType=" + portType + ", operation=" + operation + ", endpointName=" + endpointName);
				nodeProperty.put("advance_error", "The advance data is error, cannot to convert to json : " + e.getMessage());
			}

		} catch (Exception e) {
			Log log = common.logging.getLogger(this);
			log.info(null, e);
			nodeProperty.put("advance_error", e.getMessage());
		}

		return nodeProperty;
	}

	/**
	 * To load copybook from config data in SSQ-Admin
	 * 
	 * @param flow
	 * @param nodeProperty
	 * @param programCallConfigName
	 * @return map
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> loadCopybook(Flow flow, Map<String, Object> nodeProperty, String programCallConfigName) {

		// try to load wsdl
		try {

			long flowId = flow.getFlowId();
			String nodeId = (String) nodeProperty.get("nodeId");
			String nodeType = (String) nodeProperty.get("nodeType");

			if (programCallConfigName != null) {
				nodeProperty.put("programCallConfigName", programCallConfigName);
			}

			// Find ewi cell in flow
			// ****************************
			EWICell cell = new EWICell(EWICell.PROGRAMCALL);
			if (cell.getPCData() == null) {
				cell.setPCData(new PCData());
			}
			// ****************************

			Map<String, Object> newProps = new HashMap<String, Object>();

			// Make data to call interface
			NodeCell nodeCell = NodeCell.create(flowId, nodeId, flow, cell, newProps);

			// Node Props
			// Load properties
			try {

				NodeProps nodeProps = FlowCommon.getNodeProp(nodeCell);
				Object outProps = nodeProps.mergePlugConfig(nodeCell, nodeProperty);

				// convert new prop
				newProps = (Map<String, Object>) outProps;

				// Add node info
				newProps.put("nodeType", nodeType);
				newProps.put("nodeId", nodeId);

				return newProps;

			} catch (Exception e) {
				Log log = common.logging.getLogger(this);
				log.info(null, e);
				log.info("programCallConfigName=" + programCallConfigName);
				nodeProperty.put("advance_error", "The advance data is error, cannot to convert to json : " + e.getMessage());
			}

		} catch (Exception e) {
			Log log = common.logging.getLogger(this);
			log.info(null, e);
			nodeProperty.put("advance_error", e.getMessage());
		}

		return nodeProperty;
	}

	/**
	 * To load refresh data (Refresh Length of Data)
	 * 
	 * @param flow
	 * @param nodeProperty
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> refreshProgramCallData(Flow flow, Map<String, Object> nodeProperty) {
		// try to load wsdl
		try {

			long flowId = flow.getFlowId();
			String nodeId = (String) nodeProperty.get("nodeId");
			String nodeType = (String) nodeProperty.get("nodeType");

			// Find ewi cell in flow
			// ****************************
			EWICell cell = new EWICell(EWICell.PROGRAMCALL);
			if (cell.getPCData() == null) {
				cell.setPCData(new PCData());
			}
			// ****************************

			Map<String, Object> newProps = nodeProperty;

			// Make data to call interface
			NodeCell nodeCell = NodeCell.create(flowId, nodeId, flow, cell, newProps);

			// Node Props
			// Load properties
			try {

				// Update data in EWICell
				NodeProps nodeProps = FlowCommon.getNodeProp(nodeCell);

				// get object props
				Object outProps = nodeProps.loadPlugConfig(nodeCell);

				// convert new prop
				newProps = (Map<String, Object>) outProps;

				// Add node info
				newProps.put("nodeType", nodeType);
				newProps.put("nodeId", nodeId);

				return newProps;

			} catch (Exception e) {
				Log log = common.logging.getLogger(this);
				log.info(null, e);
				nodeProperty.put("advance_error", "The advance data is error, cannot to convert to json : " + e.getMessage());
			}

		} catch (Exception e) {
			Log log = common.logging.getLogger(this);
			log.info(null, e);
			nodeProperty.put("advance_error", e.getMessage());
		}

		return nodeProperty;
	}

	/**
	 * List field by screen name
	 * 
	 * @param screenName
	 * @return
	 */
	public List<Map<String, Object>> listFieldInScreen(String screenName) {

		List<FieldBean> fields = common.ssquare.screen.listFieldByScreen(screenName);
		List<Map<String, Object>> listField = new ArrayList<Map<String, Object>>();

		if (fields != null) {
			for (FieldBean fb : fields) {
				String fname = fb.getName();
				String fpath = fname;
				String fieldType = DataTypeConsts.TYPE_STRING;
				boolean isCollection = fb.isCollection();
				if (isCollection) {
					fieldType = DataTypeConsts.TYPE_ARRAY;
				}
				Map<String, Object> field = RootDataUtil.makeField(fname, fpath, fieldType, DataTypeConsts.CREATE_TYPE_AUTO, "", true, true);
				listField.add(field);

				if (isCollection) {					
					List<FieldBean> listChildren = common.ssquare.screen.listChildrenField(screenName, fname);
					if(listChildren != null) {
						List<Map<String, Object>> children = new ArrayList<Map<String,Object>>();
						field.put("children", children);
						
						for (FieldBean cfb : listChildren) {
							String cfname = cfb.getName();
							String cfpath = fname+"/"+cfname;
							String cfieldType = DataTypeConsts.TYPE_STRING;
							Map<String, Object> cfield = RootDataUtil.makeField(cfname, cfpath, cfieldType, DataTypeConsts.CREATE_TYPE_AUTO, "", true, true);
							children.add(cfield);
						}
					}
				}
			}
		}

		return listField;
	}

}
