package com.ssquare.common;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;

import com.mxgraph.model.mxCell;
import com.ssquare.consts.DataTypeConsts;
import com.ssquare.facade.common.Common;
import com.ssquare.facade.common.config.PropConfig;
import com.ssquare.facade.common.handler.ssquare.Flow;
import com.ssquare.facade.common.handler.ssquare.data.NodeCell;
import com.ssquare.property.NodeProps;
import com.ssquare.property.node.Brms;
import com.ssquare.property.node.CicsCall;
import com.ssquare.property.node.Code;
import com.ssquare.property.node.Database;
import com.ssquare.property.node.Decision;
import com.ssquare.property.node.End;
import com.ssquare.property.node.External;
import com.ssquare.property.node.Key;
import com.ssquare.property.node.Menu;
import com.ssquare.property.node.MultiDecision;
import com.ssquare.property.node.MultiSubService;
import com.ssquare.property.node.ProgramCall;
import com.ssquare.property.node.Screen;
import com.ssquare.property.node.SelectionList;
import com.ssquare.property.node.Start;
import com.ssquare.property.node.SubService;
import com.ssquare.property.node.WebService;
import com.ssquare.util.MapUtil;
import com.ssquare.util.RootDataUtil;
import com.ssquare.util.WidgetsUtil;
import com.wds.ssquare.jsonpath.JsonPathEditor;

import techberry.ewi.graph.editor.EWICell;

public class FlowCommon extends Common {

	/**
	 * You method of any accessor have one parameter, it is instance of
	 * ServiceAccessor
	 * 
	 * @param accessor
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> loadSSQNode(String search) throws Exception {

		// To get logger
		Log logger = common.logging.getLogger(this);

		// Stamp log to start
		logger.debug("loadCommonNode start");

		// Get default props
		Map<String, Object> widgetMap = WidgetsUtil.getWidgets();

		if (widgetMap != null) {
			List<Map<String, Object>> allWidgets = (List<Map<String, Object>>) widgetMap.get("widgets");

			if (allWidgets == null) {
				allWidgets = new ArrayList<Map<String, Object>>();
			}

			if (search == null || search.isEmpty() || allWidgets.isEmpty()) {
				return setWidgetStyle(allWidgets);
			} else {
				search = search.toLowerCase();

				ArrayList<Map<String, Object>> filterWidget = new ArrayList<Map<String, Object>>();
				for (Map<String, Object> node : allWidgets) {
					String nodeType = (String) node.get("nodeType");
					nodeType = nodeType.toLowerCase();
					if (nodeType.contains(search)) {
						filterWidget.add(node);
					}
				}

				return setWidgetStyle(filterWidget);
			}
		}

		logger.debug("loadCommonNode end");

		return new ArrayList<Map<String, Object>>();
	}

	/**
	 * To change widgets style
	 * 
	 * @param widgets
	 * @return
	 * @throws IOException
	 */
	private List<Map<String, Object>> setWidgetStyle(List<Map<String, Object>> widgets) throws IOException {

		if (widgets != null && !widgets.isEmpty()) {

			String stylePath = common.getProp().getValue("path.ssquare.nodeStyle");

			PropConfig propConf = common.getProp(new File(stylePath), "UTF-8");

			for (Map<String, Object> widget : widgets) {

				String nodeType = (String) widget.get("nodeType");
				String nodeIcon = (String) widget.get("nodeIcon");

				String style = propConf.getValue(nodeType);
				if (style == null) {
					style = "";
				}

				if (nodeIcon != null && !nodeIcon.trim().isEmpty()) {
					style = style + ";image=" + nodeIcon;
				}

				widget.put("style", style);
				widget.put("systemType", "SSQUARE");
			}

		}

		return widgets;
	}

	/**
	 * This common code to set property
	 * 
	 * @param flow
	 * @param propModel
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public void setNodeProperties(Flow flow, Map<String, Object> propModel) throws Exception {

		// To get logger
		Log logger = common.logging.getLogger(this);
		// Stamp log to start
		logger.debug("setNodeProperties start");

		// Loop to set
		if (propModel != null) {

			Iterator<String> iter = propModel.keySet().iterator();
			while (iter.hasNext()) {

				long flowId = flow.getFlowId();
				String nodeId = iter.next();

				Object objProps = propModel.get(nodeId);
				if (!(objProps instanceof Map)) {
					// ignore wrong format props
					continue;
				}
				Map<String, Object> props = (Map<String, Object>) objProps;
				String nodeType = (String) props.get("nodeType");

				// try to save flow
				try {

					// Find ewi cell in flow
					EWICell cell = flow.getEWICell(nodeId, nodeType);

					// Common data
					String codeString = (String) props.get("codeString");
					String nodeTitle = (String) props.get("nodeTitle");
					String nodeDesc = (String) props.get("nodeDesc");

					// Set common data
					cell.setCodeString(codeString);
					cell.setNodeTitle(nodeTitle);
					cell.setNodeDesc(nodeDesc);

					// Set target
					// Decision have to set difference
					if (!nodeType.endsWith("DECISION")) {
						List<EWICell> targetCells = flow.getTargetCell(cell);
						if (!targetCells.isEmpty()) {
							for (int i = 0; i < targetCells.size(); i++) {
								EWICell target = targetCells.get(i);
								cell.setConnectionCell(target, i);
							}
						}
					}

					// Make data to call interface
					NodeCell nodeCell = NodeCell.create(flowId, nodeId, flow, cell, props);

					// Node Props
					// Save properties
					NodeProps nodeProps = getNodeProp(nodeCell);
					if (nodeProps != null) {
						logger.info(nodeProps.getClass() + ",  nodeid = " + nodeId);
						nodeProps.saveProperty(nodeCell);
					} else {
						logger.info("No nodeProps! nodeid = " + nodeId);
					}

				} catch (Exception e) {
					logger.debug(e.getMessage());
				}

			}

		} else {
			throw new Exception("propModel is null");
		}

		logger.debug("setNodeProperties end");
	}

	/**
	 * To get graphModel and set style to Canvas
	 * 
	 * @param flow
	 * @return graphModel
	 * @throws Exception
	 */
	public String getGraphModel(Flow flow) throws Exception {

		// All cells
		List<EWICell> listCell = flow.getEWICells();

		// All widgets
		List<Map<String, Object>> widgets = loadSSQNode("");

		if (listCell != null && !listCell.isEmpty() && widgets != null && !widgets.isEmpty()) {

			for (EWICell cell : listCell) {
				String cellType = cell.getCellType();
				for (Map<String, Object> widget : widgets) {
					String nodeType = (String) widget.get("nodeType");
					if (cellType.equals(nodeType)) {
						String style = (String) widget.get("style");
						cell.setStyle(style);
						break;
					} else if (cellType.contains("DECISION") && nodeType.equals("DECISION")) {
						String style = (String) widget.get("style");
						cell.setStyle(style);
						break;
					}
				}

			}

		}

		// All Edges
		List<mxCell> listEdge = flow.getEdges();
		if (listEdge != null) {

			String stylePath = common.getProp().getValue("path.ssquare.nodeStyle");
			PropConfig propConf = common.getProp(new File(stylePath), "UTF-8");
			String edgeStyle = propConf.getValue("EDGE");

			// Loop to set edge style
			for (mxCell cell : listEdge) {
				cell.setStyle(edgeStyle);
			}
		}

		return flow.getGraphModel();
	}

	/**
	 * To get prop model
	 * 
	 * @param flow
	 * @return propModel
	 * @throws Exception
	 */
	public Map<String, Object> getPropModel(Flow flow) throws Exception {

		Map<String, Object> propModel = new HashMap<String, Object>();

		// All cells
		List<EWICell> listCell = flow.getEWICells();
		if (listCell != null) {

			for (EWICell cell : listCell) {

				// Initial
				long flowId = flow.getFlowId();
				String nodeId = cell.getItemId();
				String nodeType = cell.getCellType();
				String value = (String) cell.getValue();
				String nodeTitle = cell.getNodeTitle();
				String nodeDesc = cell.getNodeDesc();
				int connectionNum = cell.getConnectionNum();
				Map<String, Object> props = new HashMap<String, Object>();

				// Common value
				props.put("nodeId", nodeId);
				props.put("nodeType", nodeType);
				props.put("value", value);
				props.put("nodeTitle", nodeTitle);
				props.put("nodeDesc", nodeDesc);

				// Other common value
				props.put("flowId", flowId);
				props.put("codeString", cell.getCodeString());
				props.put("connectionNum", connectionNum);
				props.put("maxLoop", cell.getMaxLoop());

				// Make data to call interface
				NodeCell nodeCell = NodeCell.create(flowId, nodeId, flow, cell, props);

				// Node Props
				// Load properties
				try {
					NodeProps nodeProps = getNodeProp(nodeCell);
					nodeProps.loadProperty(nodeCell);
				} catch (Exception e) {
					Log log = common.logging.getLogger(this);
					log.info(null, e);
					props.put("advance_error", "The advance data is error, cannot to convert to json");
				}

				// Keep data to propModel
				propModel.put(nodeId, props);
			}
		}

		return propModel;
	}

	/**
	 * Get node prop by cell
	 * 
	 * @param cell
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static NodeProps getNodeProp(NodeCell nodeCell) {

		NodeProps nodeProps = null;
		EWICell cell = nodeCell.cell;
		Map<String, Object> props = nodeCell.props;

		String cellType = cell.getCellType();

		// if it is decision
		if (cellType.equals(EWICell.DECISION)) {
			List<Object> conditions = (List<Object>) props.get("conditions");
			if (conditions != null && conditions.size() > 2) {
				cellType = EWICell.MULTIDECISION;
				cell.setCellType(cellType);
			}
		}

		if (cellType.equals(EWICell.STARTFLOW)) {
			nodeProps = new Start();
		} else if (cellType.equals(EWICell.ENDFLOW)) {
			nodeProps = new End();
		} else if (cellType.equals(EWICell.CODE)) {
			nodeProps = new Code();
		} else if (cellType.equals(EWICell.SCREEN)) {
			nodeProps = new Screen();
		} else if (cellType.equals(EWICell.KEY)) {
			nodeProps = new Key();
		} else if (cellType.equals(EWICell.DECISION)) {
			nodeProps = new Decision();
		} else if (cellType.equals(EWICell.MULTIDECISION)) {
			nodeProps = new MultiDecision();
		} else if (cellType.equals(EWICell.DATABASE)) {
			nodeProps = new Database();
		} else if (cellType.equals(EWICell.SELECTIONLIST)) {
			nodeProps = new SelectionList();
		} else if (cellType.equals(EWICell.MENU)) {
			nodeProps = new Menu();
		} else if (cellType.equals(EWICell.BRMS)) {
			nodeProps = new Brms();
		} else if (cellType.equals(EWICell.WEBSERVICE)) {
			nodeProps = new WebService();
		} else if (cellType.equals(EWICell.PROGRAMCALL)) {
			nodeProps = new ProgramCall();
		} else if (cellType.equals(EWICell.CICSCALL)) {
			nodeProps = new CicsCall();
		} else if (cellType.equals(EWICell.SERVICE)) {
			nodeProps = new SubService();
		} else if (cellType.equals(EWICell.MULTISERVICE)) {
			nodeProps = new MultiSubService();
		} else if (cellType.equals(EWICell.EXTERNAL)) {
			nodeProps = new External();
		}

		return nodeProps;
	}

	/**
	 * To list field in data
	 * 
	 * @param flow
	 * @param propModel
	 * @param dataListOnly
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> listFieldName(Flow flow, Map<String, Object> propModel, boolean isDataList, List<Map<String, Object>> promoteModel) throws Exception {

		// To get logger
		Log logger = common.logging.getLogger(this);
		// Stamp log to start
		logger.debug("listFieldName start");

		Map<String, Object> buffer = new HashMap<String, Object>();

		if (propModel != null) {

			Iterator<String> iter = propModel.keySet().iterator();
			long flowId = flow.getFlowId();
			while (iter.hasNext()) {

				String nodeId = iter.next();

				Object nodeObj = propModel.get(nodeId);

				// Property is instance of Map
				if (nodeObj instanceof Map) {

					Map<String, Object> props = (Map<String, Object>) nodeObj;

					String nodeType = (String) props.get("nodeType");

					// Make cell
					EWICell cell = new EWICell(nodeType);

					// if it is decision
					if (nodeType.equals(EWICell.DECISION)) {
						List<Object> conditions = (List<Object>) props.get("conditions");
						if (conditions != null && conditions.size() > 2) {
							cell = new EWICell(EWICell.MULTIDECISION);
						}
					}

					// Make data to call interface
					NodeCell nodeCell = NodeCell.create(flowId, nodeId, flow, cell, props);

					// Node Props
					// Save properties
					NodeProps nodeProps = getNodeProp(nodeCell);
					if (nodeProps != null) {

						logger.info(nodeProps.getClass() + ",  nodeid = " + nodeId);

						Map<String, Object> fields = null;

						try {

							if (isDataList) {
								fields = nodeProps.listDataListName(nodeCell);
							} else {
								fields = nodeProps.listFieldName(nodeCell);
							}

						} catch (Exception e) {
							logger.info("error at nodeId=" + nodeId + "\n", e);
						}

						if (fields != null && !fields.isEmpty()) {
							buffer.putAll(fields);
						}

					} else {
						logger.info("No nodeProps! nodeid = " + nodeId);
					}

				}
			}

		} else {
			throw new Exception("propModel is null!");
		}

		List<Map<String, Object>> list = MapUtil.mapToArray(buffer);

		// Find field in promote
		if (promoteModel != null && !promoteModel.isEmpty()) {
			RootDataUtil.listFieldNameInPromote(promoteModel, list, isDataList);
		}

		// Stamp log to start
		logger.debug("listFieldName end");

		return list;
	}

	/**
	 * To list field in dataList
	 * 
	 * @param flow
	 * @param propModel
	 * @param dataListOnly
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> listFieldNameInDataList(Flow flow, Map<String, Object> propModel, String listName) throws Exception {

		// To get logger
		Log logger = common.logging.getLogger(this);
		// Stamp log to start
		logger.debug("listFieldNameInDataList start");

		Map<String, Object> buffer = new HashMap<String, Object>();

		if (propModel != null) {

			Iterator<String> iter = propModel.keySet().iterator();
			long flowId = flow.getFlowId();
			while (iter.hasNext()) {

				String nodeId = iter.next();

				Object nodeObj = propModel.get(nodeId);

				// Property is instance of Map
				if (nodeObj instanceof Map) {

					Map<String, Object> props = (Map<String, Object>) nodeObj;

					String nodeType = (String) props.get("nodeType");

					// Make cell
					EWICell cell = new EWICell(nodeType);

					// Make data to call interface
					NodeCell nodeCell = NodeCell.create(flowId, nodeId, flow, cell, props);

					// Node Props
					// Save properties
					NodeProps nodeProps = getNodeProp(nodeCell);
					if (nodeProps != null) {

						logger.info(nodeProps.getClass() + ",  nodeid = " + nodeId);

						Map<String, Object> fields = null;

						try {

							fields = nodeProps.listFieldNameInDataList(nodeCell, listName);

						} catch (Exception e) {
							logger.info("error at nodeId=" + nodeId + "\n", e);
						}

						if (fields != null && !fields.isEmpty()) {
							buffer.putAll(fields);
						}

					} else {
						logger.info("No nodeProps! nodeid = " + nodeId);
					}

				}
			}

		} else {
			throw new Exception("propModel is null!");
		}

		List<Map<String, Object>> list = MapUtil.mapToArray(buffer);

		// Stamp log to start
		logger.debug("listFieldNameInDataList end");

		return list;
	}

	/**
	 * To Load promote field data
	 * 
	 * @param flow
	 * @param propModel
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> loadPromoteModel(Flow flow, Map<String, Object> propModel, List<Map<String, Object>> promoteModel) throws Exception {

		// To get logger
		Log logger = common.logging.getLogger(this);
		// Stamp log to start
		logger.info("loadPromoteModel start");

		Map<String, Object> buffer = new HashMap<String, Object>();

		if (propModel != null) {

			Iterator<String> iter = propModel.keySet().iterator();
			long flowId = flow.getFlowId();
			while (iter.hasNext()) {

				String nodeId = iter.next();

				Object nodeObj = propModel.get(nodeId);

				// Property is instance of Map
				if (nodeObj instanceof Map) {

					Map<String, Object> props = (Map<String, Object>) nodeObj;

					String nodeType = (String) props.get("nodeType");

					// Make cell
					EWICell cell = new EWICell(nodeType);

					// Make data to call interface
					NodeCell nodeCell = NodeCell.create(flowId, nodeId, flow, cell, props);

					// Node Props
					// Save properties
					NodeProps nodeProps = getNodeProp(nodeCell);
					if (nodeProps != null) {

						logger.debug(nodeProps.getClass() + ",  nodeid = " + nodeId);

						Map<String, Object> fields = null;

						try {

							fields = nodeProps.listFieldName(nodeCell);
							Map<String, Object> dataList = nodeProps.listDataListName(nodeCell);
							String listName = null;

							if (dataList != null && !dataList.isEmpty()) {
								Iterator<String> subIter = dataList.keySet().iterator();
								while (subIter.hasNext()) {

									listName = subIter.next();
									Map<String, Object> subMap = (Map<String, Object>) dataList.get(listName);
									Map<String, Object> subfields = nodeProps.listFieldNameInDataList(nodeCell, listName);

									if (subfields != null) {
										List<Map<String, Object>> subfieldList = MapUtil.mapToArray(subfields);
										subMap.put("children", subfieldList);
									} else {
										subMap.put("children", new ArrayList<Map<String, Object>>());
									}

									fields.put(listName, subMap);
								}

								// logger.info("dataList = " + dataList);
							}

						} catch (Exception e) {
							logger.info("error at nodeId=" + nodeId + "\n", e);
						}

						if (fields != null && !fields.isEmpty()) {
							// logger.info("fields = " + fields);
							MapUtil.putAllFieldToMap(buffer, fields);
						}

					} else {
						logger.info("No nodeProps! nodeid = " + nodeId);
					}

				}
			}

		} else {
			throw new Exception("propModel is null!");
		}

		// new Props Model
		// content data
		// logger.info("buffer:" + buffer);
		List<Map<String, Object>> contentData = MapUtil.mapToArray(buffer);

		// root data
		// logger.info("contentData:" + contentData);
		List<Map<String, Object>> newPropModel = RootDataUtil.getRootSSQData(null, contentData);

		// Merge data with old data
		// logger.info("newPropModel:" + newPropModel);
		if (promoteModel != null) {
			newPropModel = RootDataUtil.mergePromoteField(newPropModel, promoteModel);
		}

		// Sort promote field
		newPropModel = RootDataUtil.sortPromoteField(newPropModel);

		// Stamp log to start
		logger.info("loadPromoteModel end");

		return newPropModel;
	}

	/**
	 * Save promoteModel to database
	 * 
	 * @param flow
	 * @param promoteModel
	 */
	public void savePromoteModel(Flow flow, List<Map<String, Object>> promoteModel) {

		Log logger = common.logging.getLogger(this);
		logger.debug("savePromoteModel start");

		List<Map<String, Object>> ssqPromoteModel = new ArrayList<Map<String, Object>>();

		try {

			// convert data
			convertToSSQPromoteModel(ssqPromoteModel, promoteModel, "ROOT");

			logger.info("ssqPromoteModel=" + ssqPromoteModel);

			// Set promote data back to flow
			flow.setPropmoteModel(ssqPromoteModel);

		} catch (Exception e) {
			logger.error("savePromoteModel error", e);
		}

		logger.debug("savePromoteModel end");
	}

	/**
	 * Convert data to ssq promote data
	 * 
	 * @param ssqPromoteModel
	 * @param data
	 * @param parent
	 */
	@SuppressWarnings({ "unchecked" })
	private void convertToSSQPromoteModel(List<Map<String, Object>> ssqPromoteModel, List<Map<String, Object>> data, String parent) {

		if (data != null && !data.isEmpty()) {
			for (Map<String, Object> map : data) {

				String name = (String) map.get("name");
				String fieldType = (String) map.get("fieldType");
				String sPath = parent + "." + name;

				Map<String, Object> ssqField = RootDataUtil.convertSsqField(map, sPath);
				ssqPromoteModel.add(ssqField);

				if (DataTypeConsts.TYPE_ARRAY.equalsIgnoreCase(fieldType)) {
					sPath = sPath + "[i]";
				}

				// Recursive to sub field
				if (DataTypeConsts.TYPE_ARRAY.equalsIgnoreCase(fieldType) || DataTypeConsts.TYPE_MAP.equalsIgnoreCase(fieldType)) {
					List<Map<String, Object>> children = (List<Map<String, Object>>) map.get("children");
					if (children != null) {
						convertToSSQPromoteModel(ssqPromoteModel, children, sPath);
					}
				}
			}
		}

	}

	/**
	 * Convert SSQ Promote Model To Canvas Promote Model
	 * 
	 * @param promoteModel
	 * @param data
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> convertToCanvasPromoteModel(Flow flow) {

		List<Map<String, Object>> canvasPromoteModel = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> ssqPromoteModel = flow.getPropmoteModel();

		// Filter ROOT.content and createType=manual only
		JsonPathEditor jpe = new JsonPathEditor();
		for (Map<String, Object> map : ssqPromoteModel) {

			String path = (String) map.get("path");
			String create_type = (String) map.get("create_type");

			if (path.startsWith("ROOT.content") && (create_type.equalsIgnoreCase("MANUAL") || create_type.equalsIgnoreCase("AUTO") || path.equals("ROOT.content"))) {
				path = removeArrayPath(path);
				if (!path.endsWith(".")) {
					jpe.setValue(path, map);
				} else {
					System.out.println(path);
				}
			}

		}

		// Convert to canvas promote model
		Map<String, Object> data = jpe.getRootMap();
		Map<String, Object> content = (Map<String, Object>) data.get("content");

		// System.out.println("DATA = " + data);
		// System.out.println("JSON DATA = "+jpe.jsonString());

		doConvertToCanvasPromoteModel(canvasPromoteModel, content);

		// System.out.println("CanvasPromoteModel = " + canvasPromoteModel);

		return canvasPromoteModel;

	}

	/**
	 * Convert data to new promote data
	 * 
	 * @param canvasPromote
	 * @param data
	 * @param parent
	 */
	private void doConvertToCanvasPromoteModel(List<Map<String, Object>> canvasPromote, Map<String, Object> data) {

		if (data != null) {

			String xpath = (String) data.get("path");
			Boolean promote_input = (Boolean) data.get("promote_input");
			Boolean promote_output = (Boolean) data.get("promote_output");
			String create_type = (String) data.get("create_type");
			String type = (String) data.get("type");
			String default_value = (String) data.get("default_value");
			String name = getFieldNamePath(xpath);

			// add field
			// System.out.println("name=" + name + ", promoteInput=" + promote_input + ",
			// promoteOutput=" + promote_output + ", crreateType=" + create_type + ", type="
			// + type + ", path=" + path);

			if (xpath != null && name != null) {

				Map<String, Object> field = RootDataUtil.makeField(name, xpath, type, create_type, default_value, promote_input, promote_output);
				canvasPromote.add(field);

				List<Map<String, Object>> children = getChildren(data);
				if (children != null && !children.isEmpty()) {

					System.out.println("children=" + children);
					List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
					field.put("children", list);

					// Loop to find children data
					for (Map<String, Object> map : children) {
						doConvertToCanvasPromoteModel(list, map);
					}
				}

			}

		}

	}

	private String getFieldNamePath(String path) {
		if (path != null) {
			int i = path.lastIndexOf(".");
			return path.substring(i + 1, path.length());
		}
		return null;
	}

	private String removeArrayPath(String path) {
		path = path.replace("[i]", ".");
		path = path.replace("..", ".");
		return path;
	}

	@SuppressWarnings("unchecked")
	private List<Map<String, Object>> getChildren(Map<String, Object> field) {
		List<Map<String, Object>> children = new ArrayList<Map<String, Object>>();

		Iterator<String> iter = field.keySet().iterator();
		while (iter.hasNext()) {
			String key = iter.next();
			Object val = field.get(key);
			if (val instanceof Map) {
				Map<String, Object> map = (Map<String, Object>) val;
				children.add(map);
			}
			// System.out.println("Key=" + key + ", value=" + val);
		}

		return children;
	}

	public static void main(String[] args) {

//		String s = "runningTime,connectedCells,screenGroup,screen";
//		String[] ss = s.split(",");
//		for (String x : ss) {
//			System.out.println(x);
//		}
//
//		// Object
//		Map<String, Object> map = new HashMap<String, Object>();
//		EWICell cell = new EWICell(EWICell.CODE);
//
//		BeanUtils.copyProperties(cell, map);
//		common.bean.copyValue(cell, map);

		FlowCommon com = new FlowCommon();
		System.out.println(com.getFieldNamePath("ROOT.com.xxx.name"));

	}

}
