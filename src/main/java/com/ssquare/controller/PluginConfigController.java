package com.ssquare.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ssquare.accessor.PluginConfigAccessor;
import com.ssquare.facade.accessor.ServiceAccessor;
import com.ssquare.facade.common.model.RequestData;
import com.ssquare.facade.common.model.ResponseData;

@Controller
@RequestMapping("/apis/msa")
public class PluginConfigController {

	/**
	 * To refresh web-service properties by 2 step 1. load data from WSDL 2. merge
	 * data from current data (from front end)
	 * 
	 * @param request
	 * @param requestData
	 * @return property of this node
	 */
	@RequestMapping(value = "/loadWSDL", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseData loadWSDL(HttpServletRequest request, @RequestBody RequestData requestData) {

		// To get logger
		Log logger = ServiceAccessor.getLogging().getLogger(this);

		logger.info("loadWSDL Start");

		// Create ServiceAccessor
		ServiceAccessor serviceAcc = new ServiceAccessor(request, requestData);

		// To do in try block
		try {

			// Call custom accessor that you implement
			PluginConfigAccessor accessor = new PluginConfigAccessor();
			accessor.loadWSDL(serviceAcc);

		} catch (Exception e) {
			logger.info(e);
			serviceAcc.throwException(e);
		}

		logger.info("loadWSDL End");

		// Return response data to front-end
		return serviceAcc.context.response;
	}

	/**
	 * To refresh data from copybook in config (admin/plugin)
	 * 
	 * @param request
	 * @param requestData
	 * @return property of this node
	 */
	@RequestMapping(value = "/loadCopybook", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseData loadCopybook(HttpServletRequest request, @RequestBody RequestData requestData) {

		// To get logger
		Log logger = ServiceAccessor.getLogging().getLogger(this);

		logger.info("loadCopybook Start");

		// Create ServiceAccessor
		ServiceAccessor serviceAcc = new ServiceAccessor(request, requestData);

		// To do in try block
		try {

			// Call custom accessor that you implement
			PluginConfigAccessor accessor = new PluginConfigAccessor();
			accessor.loadCopybook(serviceAcc);

		} catch (Exception e) {
			logger.info(e);
			serviceAcc.throwException(e);
		}

		logger.info("loadCopybook End");

		// Return response data to front-end
		return serviceAcc.context.response;
	}

	/**
	 * To refresh data to calculate offset of field
	 * 
	 * @param request
	 * @param requestData
	 * @return property of this node
	 */
	@RequestMapping(value = "/refreshProgramCallData", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseData refreshProgramCallData(HttpServletRequest request, @RequestBody RequestData requestData) {

		// To get logger
		Log logger = ServiceAccessor.getLogging().getLogger(this);

		logger.info("refreshProgramCallData Start");

		// Create ServiceAccessor
		ServiceAccessor serviceAcc = new ServiceAccessor(request, requestData);

		// To do in try block
		try {

			// Call custom accessor that you implement
			PluginConfigAccessor accessor = new PluginConfigAccessor();
			accessor.refreshProgramCallData(serviceAcc);

		} catch (Exception e) {
			logger.info("internal error! ", e);
			serviceAcc.throwException(e);
		}

		logger.info("refreshProgramCallData End");

		// Return response data to front-end
		return serviceAcc.context.response;
	}
	
	
	/**
	 * To refresh data to calculate offset of field
	 * 
	 * @param request
	 * @param requestData
	 * @return property of this node
	 */
	@RequestMapping(value = "/listFieldInScreen", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseData listFieldInScreen(HttpServletRequest request, @RequestBody RequestData requestData) {

		// To get logger
		Log logger = ServiceAccessor.getLogging().getLogger(this);

		logger.info("listFieldInScreen Start");

		// Create ServiceAccessor
		ServiceAccessor serviceAcc = new ServiceAccessor(request, requestData);

		// To do in try block
		try {

			// Call custom accessor that you implement
			PluginConfigAccessor accessor = new PluginConfigAccessor();
			accessor.listFieldInScreen(serviceAcc);

		} catch (Exception e) {
			logger.info("internal error! ", e);
			serviceAcc.throwException(e);
		}

		logger.info("listFieldInScreen End");

		// Return response data to front-end
		return serviceAcc.context.response;
	}

}
