package com.ssquare.controller;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;

/**
 * This class to tell springBoot that it is controller package
 * 
 * @author adirak-vdi
 *
 */
@SpringBootApplication
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
public class MainApplication {

	/**
	 * Don't edit this code
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		 //SpringApplication.run(MainApplication.class, args);

		SpringApplicationBuilder builder = new SpringApplicationBuilder(MainApplication.class);
		builder.headless(false).run(args);
	}
}