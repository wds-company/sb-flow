package com.ssquare.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ssquare.accessor.FlowAcessor;
import com.ssquare.facade.accessor.ServiceAccessor;
import com.ssquare.facade.common.model.RequestData;
import com.ssquare.facade.common.model.ResponseData;

@Controller
@RequestMapping("/apis/msa")
public class FlowController {

	/**
	 * Load common nod
	 * 
	 * @param request
	 * @param requestData
	 * @return
	 */
	@RequestMapping(value = "/loadSSQNode", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseData loadSSQNode(HttpServletRequest request, @RequestBody RequestData requestData) {

		// To get logger
		Log logger = ServiceAccessor.getLogging().getLogger(this);

		logger.info("loadSSQNode Start");

		// Create ServiceAccessor
		ServiceAccessor serviceAcc = new ServiceAccessor(request, requestData);

		// To do in try block
		try {

			// Call custom accessor that you implement
			FlowAcessor accessor = new FlowAcessor();
			accessor.loadSSQNode(serviceAcc);

		} catch (Exception e) {
			logger.info("error occur : ", e);
			serviceAcc.throwException(e);
		}

		logger.info("loadSSQNode End");

		// Return response data to front-end
		return serviceAcc.context.response;
	}

	/**
	 * Load common nod
	 * 
	 * @param request
	 * @param requestData
	 * @return
	 */
	@RequestMapping(value = "/commitFlow", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseData commitFlow(HttpServletRequest request, @RequestBody RequestData requestData) {

		// To get logger
		Log logger = ServiceAccessor.getLogging().getLogger(this);

		logger.info("commitFlow Start");

		// Create ServiceAccessor
		ServiceAccessor serviceAcc = new ServiceAccessor(request, requestData);

		// To do in try block
		try {

			// Call custom accessor that you implement
			FlowAcessor accessor = new FlowAcessor();
			accessor.commitFlow(serviceAcc);

		} catch (Exception e) {
			logger.info("error occur : ", e);
			serviceAcc.throwException(e);
		}

		logger.info("commitFlow End");

		// Return response data to front-end
		return serviceAcc.context.response;
	}

	/**
	 * Load common nod
	 * 
	 * @param request
	 * @param requestData
	 * @return
	 */
	@RequestMapping(value = "/loadFlow", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseData loadFlow(HttpServletRequest request, @RequestBody RequestData requestData) {

		// To get logger
		Log logger = ServiceAccessor.getLogging().getLogger(this);

		logger.info("loadFlow Start");

		// Create ServiceAccessor
		ServiceAccessor serviceAcc = new ServiceAccessor(request, requestData);

		// To do in try block
		try {

			// Call custom accessor that you implement
			FlowAcessor accessor = new FlowAcessor();
			accessor.loadFlow(serviceAcc);

		} catch (Exception e) {
			logger.info("error occur : ", e);
			serviceAcc.throwException(e);
		}

		logger.info("loadFlow End");

		// Return response data to front-end
		return serviceAcc.context.response;
	}
	
	/**
	 * Load common nod
	 * 
	 * @param request
	 * @param requestData
	 * @return
	 */
	@RequestMapping(value = "/loadNodeInfo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseData loadNodeInfo(HttpServletRequest request, @RequestBody RequestData requestData) {

		// To get logger
		Log logger = ServiceAccessor.getLogging().getLogger(this);

		logger.info("loadNodeInfo Start");

		// Create ServiceAccessor
		ServiceAccessor serviceAcc = new ServiceAccessor(request, requestData);

		// To do in try block
		try {

			// Call custom accessor that you implement
			FlowAcessor accessor = new FlowAcessor();
			accessor.loadNodeInfo(serviceAcc);

		} catch (Exception e) {
			logger.info("error occur : ", e);
			serviceAcc.throwException(e);
		}

		logger.info("loadNodeInfo End");

		// Return response data to front-end
		return serviceAcc.context.response;
	}

	/**
	 * To load normal field in the flow
	 * 
	 * @param request
	 * @param requestData
	 * @return property of this node
	 */
	@RequestMapping(value = "/listFieldName", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseData loadFieldInFlow(HttpServletRequest request, @RequestBody RequestData requestData) {

		// To get logger
		Log logger = ServiceAccessor.getLogging().getLogger(this);

		logger.info("listFieldName Start");

		// Create ServiceAccessor
		ServiceAccessor serviceAcc = new ServiceAccessor(request, requestData);

		// To do in try block
		try {

			// Call custom accessor that you implement
			FlowAcessor accessor = new FlowAcessor();
			accessor.listFieldName(serviceAcc);

		} catch (Exception e) {
			logger.info("error occur : ", e);
			serviceAcc.throwException(e);
		}

		logger.info("listFieldName End");

		// Return response data to front-end
		return serviceAcc.context.response;
	}

	/**
	 * To load data list in the flow
	 * 
	 * @param request
	 * @param requestData
	 * @return property of this node
	 */
	@RequestMapping(value = "/listDataListName", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseData listDataListName(HttpServletRequest request, @RequestBody RequestData requestData) {

		// To get logger
		Log logger = ServiceAccessor.getLogging().getLogger(this);

		logger.info("listDataListName Start");

		// Create ServiceAccessor
		ServiceAccessor serviceAcc = new ServiceAccessor(request, requestData);

		// To do in try block
		try {

			// Call custom accessor that you implement
			FlowAcessor accessor = new FlowAcessor();
			accessor.listDataListName(serviceAcc);

		} catch (Exception e) {
			logger.info("error occur : ", e);
			serviceAcc.throwException(e);
		}

		logger.info("listDataListName End");

		// Return response data to front-end
		return serviceAcc.context.response;
	}

	/**
	 * To load data list in the flow
	 * 
	 * @param request
	 * @param requestData
	 * @return property of this node
	 */
	@RequestMapping(value = "/listFieldNameInDataList", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseData listFieldNameInDataList(HttpServletRequest request, @RequestBody RequestData requestData) {

		// To get logger
		Log logger = ServiceAccessor.getLogging().getLogger(this);

		logger.info("listFieldNameInDataList Start");

		// Create ServiceAccessor
		ServiceAccessor serviceAcc = new ServiceAccessor(request, requestData);

		// To do in try block
		try {

			// Call custom accessor that you implement
			FlowAcessor accessor = new FlowAcessor();
			accessor.listFieldNameInDataList(serviceAcc);

		} catch (Exception e) {
			logger.info("error occur : ", e);
			serviceAcc.throwException(e);
		}

		logger.info("listFieldNameInDataList End");

		// Return response data to front-end
		return serviceAcc.context.response;
	}
	
	
	/**
	 * To load promote field in the flow
	 * 
	 * @param request
	 * @param requestData
	 * @return property of this node
	 */
	@RequestMapping(value = "/listPromoteField", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseData listPromoteField(HttpServletRequest request, @RequestBody RequestData requestData) {

		// To get logger
		Log logger = ServiceAccessor.getLogging().getLogger(this);

		logger.info("listPromoteField Start");

		// Create ServiceAccessor
		ServiceAccessor serviceAcc = new ServiceAccessor(request, requestData);

		// To do in try block
		try {

			// Call custom accessor that you implement
			FlowAcessor accessor = new FlowAcessor();
			accessor.listPromoteField(serviceAcc);

		} catch (Exception e) {
			logger.info("error occur : ", e);
			serviceAcc.throwException(e);
		}

		logger.info("listPromoteField End");

		// Return response data to front-end
		return serviceAcc.context.response;
	}
	
	/**
	 * To load promote field in the flow
	 * 
	 * @param request
	 * @param requestData
	 * @return property of this node
	 */
	@RequestMapping(value = "/splitPromoteField", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseData splitPromoteField(HttpServletRequest request, @RequestBody RequestData requestData) {

		// To get logger
		Log logger = ServiceAccessor.getLogging().getLogger(this);

		logger.info("splitPromoteField Start");

		// Create ServiceAccessor
		ServiceAccessor serviceAcc = new ServiceAccessor(request, requestData);

		// To do in try block
		try {

			// Call custom accessor that you implement
			FlowAcessor accessor = new FlowAcessor();
			accessor.splitPromoteField(serviceAcc);

		} catch (Exception e) {
			logger.info("error occur : ", e);
			serviceAcc.throwException(e);
		}

		logger.info("splitPromoteField End");

		// Return response data to front-end
		return serviceAcc.context.response;
	}
	
	

}
