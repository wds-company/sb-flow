package com.ssquare.property;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.ssquare.consts.DataTypeConsts;
import com.ssquare.facade.common.Common;
import com.ssquare.facade.common.config.PropConfig;
import com.ssquare.facade.common.handler.ssquare.data.NodeCell;
import com.ssquare.facade.common.interface_ssq.NodePropertyInterface;
import com.ssquare.util.RootDataUtil;
import com.ssquare.util.WidgetsUtil;

import blueprint.util.StringUtil;
import techberry.ewi.graph.editor.EWICell;

public abstract class NodeProps extends Common implements NodePropertyInterface {

	/**
	 * Update cell value
	 * 
	 * @param cell
	 */
	protected void updateNodeLabel(EWICell cell) {
		String nodeType  = cell.getCellType();
		String title = cell.getNodeTitle();
		String nodeName = WidgetsUtil.getNodeNameByType(nodeType);
		if(nodeName == null) {
			nodeName = nodeType;
		}
		if (title != null && !title.trim().isEmpty()) {
			cell.setValue(nodeName+"\n"+title);
		}else {
			cell.setValue(nodeName);
		}
	}

	/**
	 * Remove ignore fields
	 * 
	 * @param map
	 * @param cellType
	 * @throws IOException
	 */
	protected void removeIgnoreField(Map<String, Object> map, EWICell cell) throws IOException {

		PropConfig props = common.getProp();
		String ignoreFields = null;

		String cellType = cell.getCellType();

		if (cellType != null) {

			if (cellType.equals(EWICell.DATABASE)) {
				ignoreFields = props.getValue("ssquare.props.ignores.dbData");
			} else if (cellType.equals(EWICell.SELECTIONLIST)) {
				ignoreFields = props.getValue("ssquare.props.ignores.selectionListData");
			} else if (cellType.equals(EWICell.MENU)) {
				ignoreFields = props.getValue("ssquare.props.ignores.menuData");
			} else if (cellType.equals(EWICell.BRMS)) {
				ignoreFields = props.getValue("ssquare.props.ignores.brmsData");
			} else if (cellType.equals(EWICell.WEBSERVICE)) {
				ignoreFields = props.getValue("ssquare.props.ignores.wsData");
			} else if (cellType.equals(EWICell.PROGRAMCALL)) {
				ignoreFields = props.getValue("ssquare.props.ignores.pcData");
			} else if (cellType.equals(EWICell.CICSCALL)) {
				ignoreFields = props.getValue("ssquare.props.ignores.ccData");
			} else if (cellType.equals(EWICell.SERVICE)) {
				ignoreFields = props.getValue("ssquare.props.ignores.svData");
			} else if (cellType.equals(EWICell.MULTISERVICE)) {
				ignoreFields = props.getValue("ssquare.props.ignores.msvData");
			}

			// Loop to remove
			if (ignoreFields != null && !ignoreFields.trim().isEmpty()) {
				String[] ignoreArr = ignoreFields.split(",");
				for (String ignore : ignoreArr) {
					map.remove(ignore);
				}
			}
		} else {
			common.logging.getLogger(this).info("cellType is null");
		}

	}

	@Override
	public Object loadPlugConfig(NodeCell arg0) throws Exception {
		// TODO Please implement on sub class
		return null;
	}

	@Override
	public Object mergePlugConfig(NodeCell arg0, Object arg1) throws Exception {
		// TODO Please implement on sub class
		return null;
	}

	@Override
	public Map<String, Object> listDataListName(NodeCell arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> listFieldName(NodeCell arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> listFieldNameInDataList(NodeCell arg0, String arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	protected String makeKey(String parentKey, String name) {
		/*
		 * if(parentKey != null) { return parentKey + "['" + name +"']"; } else { return
		 * "['" + name +"']"; }
		 */
		return makeKey(UUID.randomUUID());
	}

	protected String makeKey(String parentKey, int index) {
		/*
		 * if(parentKey != null) { return parentKey + "[" + index +"]"; } else { return
		 * "[" + index +"]"; }
		 */
		return makeKey(UUID.randomUUID());
	}

	protected String makeKey(UUID uuid) {
		return uuid.toString();
	}

	/**
	 * To find field in the props data
	 * 
	 * @param props
	 * @param inOutName
	 * @param fieldName
	 * @param groupName
	 * @param checkListName
	 * @param isList
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected Map<String, Object> findField(Map<String, Object> props, String findAtName, String fieldName, String groupName, String checkListName, boolean isList, boolean promoteIn, boolean promoteOut) {

		HashMap<String, Object> fields = new HashMap<String, Object>();

		Object value = props.get(findAtName);
		if (value instanceof List) {
			List<Map<String, Object>> data = (List<Map<String, Object>>) value;
			doFindField(data, fields, fieldName, groupName, checkListName, "", isList, promoteIn, promoteOut);
		}

		return fields;
	}

	/**
	 * Loop to find field in the props data
	 * 
	 * @param data
	 * @param fields
	 * @param fieldName
	 * @param groupName
	 * @param checkListName
	 * @param path
	 * @param isList
	 */
	@SuppressWarnings("unchecked")
	private void doFindField(List<Map<String, Object>> data, HashMap<String, Object> fields, String fieldName, String groupName, String checkListName, String path, boolean isList, boolean promoteIn, boolean promoteOut) {
		if (data != null) {
			for (Map<String, Object> map : data) {

				// Defined field name
				String fname = (String) map.get(fieldName);
				if (fname == null) {
					continue;
				}

				String fpath = null;
				if (StringUtil.isBlank(path)) {
					fpath = fname;
				} else {
					fpath = path + "." + fname;
				}

				// Defined check list
				boolean checkList = false;
				try {
					checkList = (Boolean) map.get(checkListName);
				} catch (Exception e) {
					checkList = false;
				}

				// Get field type
				String fieldType = (String) map.get("fieldType");

				// Find Data List Field
				if (isList && checkList || !isList && !checkList) {

					boolean canAdd = true;

					if (checkList) {
						fieldType = DataTypeConsts.TYPE_ARRAY;
					} else if (DataTypeConsts.TYPE_MAP.equals(fieldType)) {
						canAdd = false;
					}

					if (canAdd) {
						Map<String, Object> field = RootDataUtil.makeField(fname, fpath, fieldType, DataTypeConsts.CREATE_TYPE_AUTO, "", promoteIn, promoteOut);
						fields.put(fname, field);
					}
				}

				// Ignore data if it's list but we need normal field
				if (!isList && checkList) {
					continue;
				}

				// Get the children field
				List<Map<String, Object>> children = (List<Map<String, Object>>) map.get("children");
				if (children != null && !children.isEmpty()) {
					// To call field to recursive
					doFindField(children, fields, fieldName, groupName, checkListName, fpath, isList, promoteIn, promoteOut);
				}

			}
		}
	}

	/**
	 * To find field in List
	 * 
	 * @param props
	 * @param inOutName
	 * @param fieldName
	 * @param groupName
	 * @param checkListName
	 * @param isList
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected Map<String, Object> findFieldInList(Map<String, Object> props, String findAtName, String fieldName, String groupName, String checkListName, String listName, boolean promoteIn, boolean promoteOut) {

		HashMap<String, Object> fields = new HashMap<String, Object>();

		Object value = props.get(findAtName);
		if (value instanceof List) {
			List<Map<String, Object>> data = (List<Map<String, Object>>) value;
			doFindFieldInList(data, fields, fieldName, groupName, checkListName, "", listName, promoteIn, promoteOut);
		}

		return fields;
	}

	/**
	 * Loop to find field in list
	 * 
	 * @param data
	 * @param fields
	 * @param fieldName
	 * @param groupName
	 * @param checkListName
	 * @param path
	 * @param isList
	 */
	@SuppressWarnings("unchecked")
	private void doFindFieldInList(List<Map<String, Object>> data, HashMap<String, Object> fields, String fieldName, String groupName, String checkListName, String path, String listName, boolean promoteIn, boolean promoteOut) {
		if (data != null) {

			for (Map<String, Object> map : data) {

				// Defined field name
				String fname = (String) map.get(fieldName);
				if (fname == null) {
					continue;
				}

				String fpath = null;
				if (StringUtil.isBlank(path)) {
					fpath = fname;
				} else {
					fpath = path + "." + fname;
				}

				// Defined check list
				boolean checkList = false;
				try {
					checkList = (Boolean) map.get(checkListName);
				} catch (Exception e) {
					checkList = false;
				}

				// Find Data List Field
				if (checkList && fname.equals(listName)) {
					// Get the children field
					List<Map<String, Object>> children = (List<Map<String, Object>>) map.get("children");
					if (children != null && !children.isEmpty()) {
						// To call field to recursive
						doFindField(children, fields, fieldName, groupName, checkListName, fpath, false, promoteIn, promoteOut);
					}

				}

			}
		}
	}

}
