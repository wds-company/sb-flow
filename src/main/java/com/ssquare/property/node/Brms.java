package com.ssquare.property.node;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.ssquare.facade.common.handler.ssquare.data.NodeCell;
import com.ssquare.property.NodeProps;

import techberry.ewi.graph.editor.BRMSData;
import techberry.ewi.graph.editor.EWICell;

public class Brms extends NodeProps {

	@Override
	public void loadProperty(NodeCell nodeCell) throws Exception {

		Map<String, Object> props = nodeCell.props;
		EWICell cell = nodeCell.cell;
		BRMSData data = cell.getBrmsData();

		// Update node lable
		updateNodeLabel(cell);

		// Cell -> Props
		copyData(data, props);
	}

	@Override
	public void saveProperty(NodeCell nodeCell) throws Exception {

		Map<String, Object> props = nodeCell.props;
		EWICell cell = nodeCell.cell;
		BRMSData data = cell.getBrmsData();
		if (data == null) {
			data = new BRMSData();
			cell.setBrmsData(data);
		}
		// Props --> Cell
		copyData(props, data);

		// Update node lable
		updateNodeLabel(cell);

	}

	/**
	 * @param srcData
	 * @param destData
	 */
	private void copyData(BRMSData srcData, Map<String, Object> destData) {
		// *********
		// BRMS Data
		// *********
		// prepare data
		int ruleServerType = srcData.getRuleServerType();
		String ruleServerName = srcData.getRuleServerName();
		String ruleName = srcData.getRuleName();
		String ruleType = srcData.getRuleType();
		String modelName = srcData.getModelName();
		String processId = srcData.getProcessId();
		boolean processFlowType = srcData.isProcessFlowType();
		boolean dynamicRuleProject = srcData.isDynamicRuleProject();
		String ruleProjectField = srcData.getRuleProjectField();
		String ruleVersionField = srcData.getRuleVersionField();

		String key = makeKey(null, "$");
		List<Map<String, Object>> inputFields = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> outputFields = new ArrayList<Map<String, Object>>();

		// prepare input field
		if (srcData.inputCount() > 0) {
			String inKey = makeKey(key, "inputFields");
			for (int i = 0; i < srcData.inputCount(); i++) {
				String parentKey = makeKey(inKey, i);
				Map<String, Object> field = srcData.getInputField(i);
				Map<String, Object> child = new LinkedHashMap<String, Object>();
				copyData(field, child, makeKey(parentKey, i));
				inputFields.add(child);
			}
		}
		// prepare output field
		if (srcData.outputCount() > 0) {
			String outKey = makeKey(key, "outputFields");
			for (int i = 0; i < srcData.outputCount(); i++) {
				String parentKey = makeKey(outKey, i);
				Map<String, Object> field = srcData.getOutputField(i);
				Map<String, Object> child = new LinkedHashMap<String, Object>();
				copyData(field, child, makeKey(parentKey, i));
				outputFields.add(child);
			}
		}

		// put data back to result
		destData.put("ruleServerType", ruleServerType);
		destData.put("ruleServerName", ruleServerName);
		destData.put("ruleName", ruleName);
		destData.put("ruleType", ruleType);
		destData.put("modelName", modelName);
		destData.put("processId", processId);
		destData.put("processFlowType", processFlowType);
		destData.put("dynamicRuleProject", dynamicRuleProject);
		destData.put("ruleProjectField", ruleProjectField);
		destData.put("ruleVersionField", ruleVersionField);
		destData.put("inputFields", inputFields);
		destData.put("outputFields", outputFields);
		destData.put("key", key);

	}

	/**
	 * @param srcData
	 * @param destData
	 * @param parentKey
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void copyData(Map srcData, Map destData, String parentKey) {
		// *********
		// BRMS Data
		// *********
		// Data
		destData.putAll(srcData);
		String screenField = (String) destData.remove("screenField");
		String mapToField = (String) destData.remove("mapToField");
		String ruleField = (String) destData.get("ruleField");
		String definedField = (String) destData.get("definedField");
		String fieldType = (String) destData.get("fieldType");
		boolean isScreenField = getBooleanValue(destData.get("isScreenField"));
		if (isScreenField) {
			destData.put("isScreenField", false);
			definedField = screenField;
		} else if (definedField == null && mapToField != null) {
			definedField = mapToField;
		}
		destData.put("definedField", definedField);
		if (ruleField == null && mapToField != null) {
			ruleField = mapToField;
			destData.put("ruleField", ruleField);
		}

		String key = makeKey(parentKey, definedField);
		destData.put("key", key);
		if (fieldType == null) {
			destData.put("fieldType", "String");
		}
		// *********

	}

	/**
	 * @param srcData
	 * @param destData
	 */
	@SuppressWarnings("unchecked")
	private void copyData(Map<String, Object> srcData, BRMSData destData) {
		// *********
		// BRMS Data
		// *********
		// prepare input data
		int ruleServerType = (int) srcData.get("ruleServerType");
		String ruleServerName = (String) srcData.get("ruleServerName");
		String ruleName = (String) srcData.get("ruleName");
		String ruleType = (String) srcData.get("ruleType");
		String modelName = (String) srcData.get("modelName");
		String processId = (String) srcData.get("processId");
		boolean processFlowType = getBooleanValue(srcData.get("processFlowType"));
		boolean dynamicRuleProject = getBooleanValue(srcData.get("dynamicRuleProject"));
		String ruleProjectField = (String) srcData.get("ruleProjectField");
		String ruleVersionField = (String) srcData.get("ruleVersionField");

		List<Map<String, Object>> inputFields = (List<Map<String, Object>>) srcData.get("inputFields");
		List<Map<String, Object>> outputFields = (List<Map<String, Object>>) srcData.get("outputFields");

		// put data to cell
		destData.setRuleServerType(ruleServerType);
		destData.setRuleServerName(ruleServerName);
		destData.setRuleName(ruleName);
		destData.setRuleType(ruleType);
		destData.setModelName(modelName);
		destData.setProcessId(processId);
		destData.setProcessFlowType(processFlowType);
		destData.setDynamicRuleProject(dynamicRuleProject);
		destData.setRuleProjectField(ruleProjectField);
		destData.setRuleVersionField(ruleVersionField);

		// put input to cell
		if (inputFields != null) {
			for (Map<String, Object> field : inputFields) {
				boolean isReturnToFrontend = true;
				boolean isScreenField = false;
				String definedField = (String) field.get("definedField");
				String screenField = "";// (String) field.get("screenField");
				String fixedValue = (String) field.get("fixedValue");
				String mapToField = (String) field.get("ruleField");
				// Add field
				destData.addInputField(isScreenField, screenField, definedField, fixedValue, mapToField, isReturnToFrontend);
			}
		}

		// put output to cell
		if (outputFields != null) {
			for (Map<String, Object> field : outputFields) {
				boolean isReturnToFrontend = true;
				boolean isScreenField = false;
				String definedField = (String) field.get("definedField");
				String screenField = "";// (String) field.get("screenField");
				String ruleField = (String) field.get("ruleField");
				// Add field
				destData.addOutputField(isScreenField, ruleField, screenField, definedField, isReturnToFrontend);
			}
		}

	}

	private boolean getBooleanValue(Object obj) {
		if (obj == null) {
			return false;
		} else if (obj instanceof String) {
			return "ture".equalsIgnoreCase((String) obj);
		} else if (obj instanceof Boolean) {
			return (boolean) obj;
		} else {
			return false;
		}
	}

	@Override
	public Map<String, Object> listDataListName(NodeCell nodeCell) {
		// No dataList
		return null;
	}

	@Override
	public Map<String, Object> listFieldName(NodeCell nodeCell) {
		// Property from font-end
		Map<String, Object> props = nodeCell.props;

		// Find list name
		Map<String, Object> data1 = findField(props, "inputFields", "definedField", "Array", "isCollection", true, true, false);
		Map<String, Object> data2 = findField(props, "outputFields", "definedField", "Array", "isCollection", true, false, true);
		data1.putAll(data2);

		return data1;
	}

	@Override
	public Map<String, Object> listFieldNameInDataList(NodeCell nodeCell, String listName) {
		// No dataList
		return null;
	}

}
