package com.ssquare.property.node;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.ssquare.consts.DataTypeConsts;
import com.ssquare.facade.common.handler.ssquare.data.NodeCell;
import com.ssquare.property.NodeProps;
import com.ssquare.util.RootDataUtil;

import techberry.ewi.data.bean.entity.FieldBean;
import techberry.ewi.data.bean.entity.ScreenBean;
import techberry.ewi.graph.editor.EWICell;

public class Screen extends NodeProps {

	@Override
	public void loadProperty(NodeCell nodeCell) throws Exception {

		EWICell cell = nodeCell.cell;
		Map<String, Object> props = nodeCell.props;
		
		// Update node lable
		updateNodeLabel(cell);

		props.put("checkScreen", cell.isCheckScreen());

		// Big Object
		if (cell.getScreen() != null) {
			props.put("screenName", cell.getScreen().getName());
		} else {
			props.put("screenName", "");
		}

		if (cell.getScreenGroup() != null) {
			props.put("screenGroupName", cell.getScreenGroup().getName());
		} else {
			props.put("screenGroupName", "");
		}
	}

	@Override
	public void saveProperty(NodeCell nodeCell) throws Exception {

		EWICell cell = nodeCell.cell;
		Map<String, Object> props = nodeCell.props;

		// Set check screen
		Boolean isCheckScreen = (Boolean) props.get("checkScreen");
		if (isCheckScreen != null) {
			cell.setCheckScreen(isCheckScreen);
		}

		String screenName = (String) props.get("screenName");
		if (screenName != null) {
			ScreenBean screen = common.ssquare.screen.getScreen(screenName);
			cell.setScreen(screen);
		}

		// Update node lable
		updateNodeLabel(cell);

	}

	@Override
	public Map<String, Object> listDataListName(NodeCell nodeCell) {
		Map<String, Object> fields = new LinkedHashMap<String, Object>();
		Map<String, Object> props = nodeCell.props;
		String screenName = (String) props.get("screenName");
		if (screenName != null) {
			List<FieldBean> list = common.ssquare.screen.listFieldByScreen(screenName);
			if (list != null) {
				for (FieldBean field : list) {
					if (field.isCollection()) {
						String name = field.getName();
						boolean promoteIn = "W".equals(field.getDirection()) || "B".equals(field.getDirection());
						boolean promoteOut = "R".equals(field.getDirection()) || "B".equals(field.getDirection());
						Map<String, Object> map = RootDataUtil.makeField(name, name, "Array", DataTypeConsts.CREATE_TYPE_AUTO, "", promoteIn, promoteOut);
						fields.put(name, map);
					}
				}
			}
		}
		return fields;
	}

	@Override
	public Map<String, Object> listFieldName(NodeCell nodeCell) {
		Map<String, Object> fields = new LinkedHashMap<String, Object>();
		Map<String, Object> props = nodeCell.props;
		String screenName = (String) props.get("screenName");
		if (screenName != null) {
			List<FieldBean> list = common.ssquare.screen.listFieldByScreen(screenName);
			if (list != null) {
				for (FieldBean field : list) {
					if (!field.isCollection()) {
						String name = field.getName();
						boolean promoteIn = "W".equals(field.getDirection()) || "B".equals(field.getDirection());
						boolean promoteOut = "R".equals(field.getDirection()) || "B".equals(field.getDirection());
						Map<String, Object> map = RootDataUtil.makeField(name, name, "String", DataTypeConsts.CREATE_TYPE_AUTO, "", promoteIn, promoteOut);
						fields.put(name, map);
					}
				}
			}
		}
		return fields;
	}

	@Override
	public Map<String, Object> listFieldNameInDataList(NodeCell nodeCell, String listName) {
		Map<String, Object> fields = new LinkedHashMap<String, Object>();
		Map<String, Object> props = nodeCell.props;
		String screenName = (String) props.get("screenName");
		if (screenName != null) {
			List<FieldBean> list = common.ssquare.screen.listChildrenField(screenName, listName);
			if (list != null) {
				for (FieldBean field : list) {
					if (!field.isCollection()) {
						String name = field.getName();
						boolean promoteIn = "W".equals(field.getDirection()) || "B".equals(field.getDirection());
						boolean promoteOut = "R".equals(field.getDirection()) || "B".equals(field.getDirection());
						Map<String, Object> map = RootDataUtil.makeField(name, name, "String", DataTypeConsts.CREATE_TYPE_AUTO, "", promoteIn, promoteOut);
						fields.put(name, map);
					}
				}
			}
		}
		return fields;
	}

}
