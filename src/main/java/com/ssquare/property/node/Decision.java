package com.ssquare.property.node;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mxgraph.model.mxCell;
import com.ssquare.facade.common.handler.ssquare.Flow;
import com.ssquare.facade.common.handler.ssquare.data.NodeCell;
import com.ssquare.property.NodeProps;

import blueprint.util.StringUtil;
import techberry.ewi.graph.editor.EWICell;

public class Decision extends NodeProps {

	@Override
	public void loadProperty(NodeCell nodeCell) throws Exception {

		Flow flow = nodeCell.flow;
		EWICell cell = nodeCell.cell;
		Map<String, Object> props = nodeCell.props;
		int connectionNum = cell.getConnectionNum();

		List<Map<String, Object>> conditions = new ArrayList<Map<String, Object>>();

		// Loop for condition
		for (int i = 0; i <= connectionNum; i++) {
			EWICell cCell = cell.getConnectionCell(i);

			Map<String, Object> edgeMap = new HashMap<String, Object>();
			String condition = cell.getConditionCode(i);
			mxCell edge = flow.getBetweenEdge(cell, cCell);

			String label = (String) edge.getValue();
			String edgeId = edge.getId();

			edgeMap.put("edgeId", edgeId);
			edgeMap.put("label", label);
			edgeMap.put("condition", condition);

			// Add edge map
			conditions.add(edgeMap);
		}

		props.put("conditions", conditions);

	}

	@SuppressWarnings("unchecked")
	@Override
	public void saveProperty(NodeCell nodeCell) throws Exception {

		Flow flow = nodeCell.flow;
		EWICell cell = nodeCell.cell;
		Map<String, Object> props = nodeCell.props;

		// Data of condition
		List<Map<String, Object>> conditions = (List<Map<String, Object>>) props.get("conditions");
		int connectionNum = conditions.size() - 1;
		int maxLoop = (Integer) props.get("maxLoop");
		if (connectionNum < 1) {
			connectionNum = 1;
		}

		// Connection data
		cell.setMaxLoop(maxLoop);
		cell.setConnectionNum(connectionNum);

		// Set cell value
		if (connectionNum < 2) {
			cell.setValue("");
		}

		// Setup condition
		if (!conditions.isEmpty()) {

			// Sort condition
			sortCondition(conditions);

			// Loop to set
			int max = conditions.size();
			for (int i = 0; i < max; i++) {

				Map<String, Object> edgeMap = conditions.get(i);

				String edgeId = (String) edgeMap.get("edgeId");
				String label = (String) edgeMap.get("label");
				String condition = (String) edgeMap.get("condition");
				mxCell edge = flow.getEdgeCellFromEdgeId(cell, edgeId);
				EWICell target = flow.getTargetCellFromEdge(edge);

				// Set connection cell at target
				cell.setConnectionCell(target, i);
				cell.setConditionCode(condition, i);

				// Update edge label
				String newLabel = getLabel(label, i, max);
				edge.setValue(newLabel);

			}

		}

	}

	/**
	 * Sort condition by empty condition must be lasted
	 * 
	 * @param conditions
	 */
	private void sortCondition(List<Map<String, Object>> conditions) {
		Collections.sort(conditions, new Comparator<Map<String, Object>>() {
			@Override
			public int compare(Map<String, Object> m1, Map<String, Object> m2) {

				String lb1 = (String) m1.get("label");
				String lb2 = (String) m2.get("label");

				if (lb1 == null) {
					lb1 = "";
				}
				if (lb2 == null) {
					lb2 = "";
				}

				String c1 = (String) m1.get("condition");
				String c2 = (String) m2.get("condition");

				if (c1 == null) {
					c1 = "";
				}
				if (c2 == null) {
					c2 = "";
				}

				c1 = c1.trim();
				c2 = c2.trim();

				if (StringUtil.isBlank(c1)) {
					lb1 = "zzzzzzzzzzzz";
				}
				if (StringUtil.isBlank(c2)) {
					lb2 = "zzzzzzzzzzzz";
				}

				return lb1.compareTo(lb2);
			}
		});
	}

	private String getLabel(String label, int i, int max) {

		if (max <= 2) {
			if (i == 0) {
				return " Yes ";
			} else {
				return " No ";
			}
		} else {

			if (i == (max - 1)) {
				return " Else ";
			} else {
				return "Condition " + (i + 1);
			}
		}

	}

	public static void main(String[] args) {
		Decision d = new Decision();

		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

		Map<String, Object> m1 = new HashMap<String, Object>();
		m1.put("condition", " x == y");
		m1.put("edgeId", "4");
		m1.put("label", "1");
		list.add(m1);

		Map<String, Object> m2 = new HashMap<String, Object>();
		m2.put("condition", "x < y");
		m2.put("edgeId", "5");
		m2.put("label", "2");
		list.add(m2);

		Map<String, Object> m3 = new HashMap<String, Object>();
		m3.put("condition", "x < y");
		m3.put("edgeId", "5");
		m3.put("label", "3");
		list.add(m3);

		System.out.println(list);

		System.out.println("++++++++++++++");
		d.sortCondition(list);

		System.out.println(list);

	}

}
