package com.ssquare.property.node;

import com.ssquare.facade.common.handler.ssquare.data.NodeCell;
import com.ssquare.property.NodeProps;

public class End extends NodeProps{

	@Override
	public void loadProperty(NodeCell nodeCell) throws Exception {
		// Don't implement
	}

	@Override
	public void saveProperty(NodeCell nodeCell) throws Exception {
		// Don't implement
	}

}
