package com.ssquare.property.node;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.ssquare.facade.common.handler.ssquare.data.NodeCell;
import com.ssquare.property.NodeProps;

import techberry.ewi.core.dbtype.ColumnType;
import techberry.ewi.graph.editor.DBData;
import techberry.ewi.graph.editor.EWICell;

public class Database extends NodeProps {

	@Override
	public void loadProperty(NodeCell nodeCell) throws Exception {

		Map<String, Object> props = nodeCell.props;
		EWICell cell = nodeCell.cell;
		DBData data = cell.getDbData();
		
		// Update node lable
		updateNodeLabel(cell);

		// Cell -> Props
		copyData(data, props);
	}

	@Override
	public void saveProperty(NodeCell nodeCell) throws Exception {

		Map<String, Object> props = nodeCell.props;
		EWICell cell = nodeCell.cell;
		DBData data = cell.getDbData();
		if (data == null) {
			data = new DBData();
			cell.setDbData(data);
		}
		// Props --> Cell
		copyData(props, data);

		// Update node lable
		updateNodeLabel(cell);

	}

	/**
	 * @param srcData
	 * @param destData
	 */
	private void copyData(DBData srcData, Map<String, Object> destData) {
		// *********
		// DB Data
		// *********
		// prepare data
		String dataSource = srcData.getDataSource();
		String sqlType = srcData.getSqlType();
		String sqlStatement = srcData.getSqlStatment();
		String savePointName = srcData.getSavePointName();
		boolean forceCommit = srcData.isForceCommit();
		String key = makeKey(null, "$");
		List<Map<String, Object>> inputFields = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> outputFields = new ArrayList<Map<String, Object>>();

		// prepare input field
		if (srcData.inputCount() > 0) {
			String inKey = makeKey(key, "inputFields");
			for (int i = 0; i < srcData.inputCount(); i++) {
				String parentKey = makeKey(inKey, i);
				Map<String, Object> field = srcData.getInputField(i);
				Map<String, Object> child = new LinkedHashMap<String, Object>();
				copyData(field, child, makeKey(parentKey, i));
				inputFields.add(child);
			}
		}
		// prepare output field
		if (srcData.outputCount() > 0) {
			String outKey = makeKey(key, "outputFields");
			for (int i = 0; i < srcData.outputCount(); i++) {
				String parentKey = makeKey(outKey, i);
				Map<String, Object> field = srcData.getOutputField(i);
				Map<String, Object> child = new LinkedHashMap<String, Object>();
				copyData(field, child, makeKey(parentKey, i));
				outputFields.add(child);
			}
		}

		// put data back to result
		destData.put("dataSource", dataSource);
		destData.put("sqlType", sqlType);
		destData.put("sqlStatement", sqlStatement);
		destData.put("savePointName", savePointName);
		destData.put("forceCommit", forceCommit);
		destData.put("inputFields", inputFields);
		destData.put("outputFields", outputFields);
		destData.put("key", key);

	}

	/**
	 * @param srcData
	 * @param destData
	 */
	@SuppressWarnings("unchecked")
	private void copyData(Map<String, Object> srcData, DBData destData) {
		// *********
		// DB Data
		// *********
		// prepare input data
		String dataSource = (String) srcData.get("dataSource");
		String sqlType = (String) srcData.get("sqlType");
		String sqlStatement = (String) srcData.get("sqlStatement");
		String savePointName = (String) srcData.get("savePointName");
		boolean forceCommit = getBooleanValue(srcData.get("forceCommit"));
		List<Map<String, Object>> inputFields = (List<Map<String, Object>>) srcData.get("inputFields");
		List<Map<String, Object>> outputFields = (List<Map<String, Object>>) srcData.get("outputFields");

		// put data to cell
		destData.setDataSource(dataSource);
		destData.setSqlType(sqlType);
		destData.setSqlStatment(sqlStatement);
		destData.setSavePointName(savePointName);
		destData.setForceCommit(forceCommit);

		// put input to cell
		if (inputFields != null) {
			for (Map<String, Object> field : inputFields) {

				boolean isScreenField = false;
				String definedField = (String) field.get("definedField");
				String screenField = "";// (String) field.get("screenField");
				String fixedValue = (String) field.get("fixedValue");
				String mapToField = (String) field.get("tableField");
				String fieldType = (String) field.get("fieldType");
				String fieldFormat = (String) field.get("fieldFormat");
				boolean isCollection = field.get("isCollection") == null ? "Array".equals(fieldType) : getBooleanValue(field.get("isCollection"));
				if (isCollection) {
					fieldType = "java.util.List";
				} else if (fieldType != null) {
					fieldType = ColumnType.getColumnTypeByName(fieldType).getClassName();
				}
				List<Map<String, String>> subFields = null;
				List<Map<String, Object>> children = (List<Map<String, Object>>) field.get("children");
				if (children != null) {
					subFields = new ArrayList<Map<String, String>>();
					copyToSubField(children, subFields);
				}
				// Add field
				destData.addInputField(isScreenField, screenField, definedField, fixedValue, mapToField, fieldType, fieldFormat, subFields, isCollection);
			}
		}

		// put output to cell
		if (outputFields != null) {
			for (Map<String, Object> field : outputFields) {

				boolean isScreenField = false;
				String definedField = (String) field.get("definedField");
				String screenField = "";// (String) field.get("screenField");
				String ruleField = (String) field.get("tableField");
				boolean isCollection = getBooleanValue(field.get("isCollection"));
				boolean isReturnToFrontend = true;
				List<Map<String, String>> subFields = null;
				List<Map<String, Object>> children = (List<Map<String, Object>>) field.get("children");
				if (children != null) {
					subFields = new ArrayList<Map<String, String>>();
					copyToSubField(children, subFields);
				}
				// Add field
				destData.addOutputField(isScreenField, ruleField, screenField, definedField, isReturnToFrontend, subFields, isCollection);
			}
		}

	}

	/**
	 * @param srcData
	 * @param destData
	 * @param parentKey
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void copyData(Map srcData, Map destData, String parentKey) {
		// *********
		// DB Data
		// *********
		// Data
		destData.putAll(srcData);
		String screenField = (String) destData.remove("screenField");
		String mapToField = (String) destData.remove("mapToField");
		String ruleField = (String) destData.remove("ruleField");
		String definedField = (String) destData.get("definedField");
		String tableField = (String) destData.get("tableField");
		String fieldType = (String) destData.get("fieldType");
		boolean isScreenField = getBooleanValue(destData.get("isScreenField"));
		if (isScreenField) {
			destData.put("isScreenField", false);
			definedField = screenField;
		} else if (definedField == null && mapToField != null) {
			definedField = mapToField;
		}
		destData.put("definedField", definedField);
		if (tableField == null) {
			if (mapToField != null) {
				tableField = mapToField;
			} else if (ruleField != null) {
				tableField = ruleField;
			}
			destData.put("tableField", tableField);
		}

		String key = makeKey(parentKey, definedField);
		destData.put("key", key);

		if (fieldType == null) {
			destData.put("fieldType", "String");
		} else if (getBooleanValue(destData.get("isCollection"))) {
			destData.put("fieldType", "Array");
		} else if (fieldType != null) {
			destData.put("fieldType", ColumnType.getColumnType(fieldType).toString());
		}

		if (!destData.containsKey("fieldFormat")) {
			destData.put("fieldFormat", "value");
		}
		// *********

		List<Map<String, String>> subFields = (List<Map<String, String>>) destData.remove("subFields");
		if (subFields != null) {
			List<Map<String, Object>> children = new ArrayList<Map<String, Object>>();
			destData.put("children", children);
			copyToChildren(subFields, children, key);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void copyToSubField(List<Map<String, Object>> children, List<Map<String, String>> subFields) {
		for (int i = 0; i < children.size(); i++) {
			Map child = (Map) children.get(i);
			Map field = new LinkedHashMap();
			field.putAll(child);
			String mapToField = (String) field.remove("definedField");
			field.put("mapToField", mapToField);
			String fieldType = (String) field.get("fieldType");
			boolean isCollection = getBooleanValue(field.get("isCollection"));
			if (fieldType != null) {
				if (isCollection) {
					fieldType = "java.util.List";
				} else {
					fieldType = ColumnType.getColumnTypeByName(fieldType).getClassName();
				}
				field.put("fieldType", fieldType);
			}
			subFields.add(field);
		}
	}

	private void copyToChildren(List<Map<String, String>> subFields, List<Map<String, Object>> children, String parentKey) {
		for (int i = 0; i < subFields.size(); i++) {
			Map<String, String> field = (Map<String, String>) subFields.get(i);
			Map<String, Object> child = new LinkedHashMap<String, Object>();
			copyData(field, child, makeKey(parentKey, i));
			children.add(child);
		}
	}

	private boolean getBooleanValue(Object obj) {
		if (obj == null) {
			return false;
		} else if (obj instanceof String) {
			return "ture".equalsIgnoreCase((String) obj);
		} else if (obj instanceof Boolean) {
			return (boolean) obj;
		} else {
			return false;
		}
	}

	@Override
	public Map<String, Object> listDataListName(NodeCell nodeCell) {

		// Property from font-end
		Map<String, Object> props = nodeCell.props;

		// Find list name
		Map<String, Object> data1 = findField(props, "inputFields", "definedField", "Array", "isCollection", true, true, false);
		Map<String, Object> data2 = findField(props, "outputFields", "definedField", "Array", "isCollection", true, false, true);
		data1.putAll(data2);

		return data1;
	}

	@Override
	public Map<String, Object> listFieldName(NodeCell nodeCell) {

		// Property from font-end
		Map<String, Object> props = nodeCell.props;

		// Find list name
		Map<String, Object> data1 = findField(props, "inputFields", "definedField", "Array", "isCollection", false, true, false);
		Map<String, Object> data2 = findField(props, "outputFields", "definedField", "Array", "isCollection", false, false, true);
		data1.putAll(data2);

		return data1;
	}

	@Override
	public Map<String, Object> listFieldNameInDataList(NodeCell nodeCell, String listName) {

		// Property from font-end
		Map<String, Object> props = nodeCell.props;

		// Find list name
		Map<String, Object> data1 = findFieldInList(props, "inputFields", "definedField", "Array", "isCollection", listName, true, false);
		Map<String, Object> data2 = findFieldInList(props, "outputFields", "definedField", "Array", "isCollection", listName, false, true);
		data1.putAll(data2);

		return data1;
	}

}
