package com.ssquare.property.node;

import java.util.Map;

import com.ssquare.facade.common.handler.Bean;
import com.ssquare.facade.common.handler.ssquare.data.NodeCell;
import com.ssquare.property.NodeProps;

import techberry.ewi.graph.editor.CCData;

public class CicsCall extends NodeProps {

	@Override
	public void loadProperty(NodeCell nodeCell) throws Exception {

		// Bean function
		Bean bean = common.bean;
		
		// Update node lable
		updateNodeLabel(nodeCell.cell);

		// Convert data of menu node
		Map<String, Object> advance = bean.toMap(nodeCell.cell.getCCData());

		// put data back to result
		nodeCell.setProp("advance", advance);

		// Remove ignore fields
		removeIgnoreField(advance, nodeCell.cell);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void saveProperty(NodeCell nodeCell) throws Exception {

		// Bean function
		Bean bean = common.bean;

		// Props
		Map<String, Object> props = nodeCell.props;

		// advance data
		Map<String, Object> advance = (Map<String, Object>) props.get("advance");
		if (advance != null) {

			// advance data
			CCData data = new CCData();

			// Copy value
			data = (CCData) bean.toObject(props, data);

			// Set data back to cell
			nodeCell.cell.setCCData(data);
		}

		// Update node lable
		updateNodeLabel(nodeCell.cell);

	}

	@Override
	public Map<String, Object> listDataListName(NodeCell arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> listFieldName(NodeCell arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> listFieldNameInDataList(NodeCell arg0, String arg1) {
		// TODO Auto-generated method stub
		return null;
	}

}
