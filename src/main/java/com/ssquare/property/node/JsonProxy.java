package com.ssquare.property.node;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.ssquare.facade.common.handler.ssquare.data.NodeCell;
import com.ssquare.property.NodeProps;

import techberry.ewi.graph.editor.EWICell;
import techberry.ewi.graph.editor.JsonNodeData;
import techberry.ewi.graph.editor.SubData;
import techberry.ewi.graph.editor.WSDataTreeType;
import techberry.ewi.graph.editor.WSTreeNode;

public class JsonProxy extends NodeProps {

	public static String TYPE_GROUP = "GROUP";
	public static String TYPE_DATA = "DATA";

	@Override
	public void loadProperty(NodeCell nodeCell) throws Exception {

		Map<String, Object> props = nodeCell.props;
		EWICell cell = nodeCell.cell;
		JsonNodeData data = cell.getJsonNodeData();
		
		// Update node lable
		updateNodeLabel(cell);

		// Cell -> Props
		copyData(data, props);

	}

	@Override
	public void saveProperty(NodeCell nodeCell) throws Exception {

		Map<String, Object> props = nodeCell.props;
		EWICell cell = nodeCell.cell;
		JsonNodeData data = cell.getJsonNodeData();
		if (data == null) {
			data = new JsonNodeData(true);
			cell.setJsonNodeData(data);
		}
		// Props --> Cell
		copyData(props, data);

		// Update node lable
		updateNodeLabel(cell);

	}

	/**
	 * @param srcData
	 * @param destData
	 */
	private void copyData(JsonNodeData srcData, Map<String, Object> destData) {
		// *********
		// Json Data
		// *********
		String endpointName = srcData.getEndpointName();
		String key = makeKey(null, "$");

		destData.put("endpointName", endpointName);
		destData.put("key", key);
		// *********

		// *********
		// Fields
		// *********
		Map<String, Object> inputFields = new LinkedHashMap<String, Object>();
		Map<String, Object> outputFields = new LinkedHashMap<String, Object>();
		WSTreeNode inputTree = srcData.getInputTree();
		WSTreeNode outputTree = srcData.getOutputTree();
		copyData(inputTree, inputFields, makeKey(key, "inputTree"));
		copyData(outputTree, outputFields, makeKey(key, "outputTree"));
		destData.put("inputFields", inputFields);
		destData.put("outputFields", outputFields);
		// *********

	}

	/**
	 * @param srcData
	 * @param destData
	 */
	private void copyData(WSTreeNode srcData, Map<String, Object> destData, String parentKey) {
		// *********
		// WS Sub Data
		// *********
		// Tree Data
		String nodeType = srcData.getNodeType().name();
		String nodeName = srcData.getNodeName();
		String key = makeKey(parentKey, nodeName);
		boolean dataList = srcData.isDataList();
		boolean inputNode = srcData.isInputNode();
		String dataType = "String";
		if (dataList) {
			dataType = "Array";
		} else if (TYPE_GROUP.equalsIgnoreCase(nodeType)) {
			dataType = "Map";
		}

		destData.put("key", key);
		destData.put("nodeType", nodeType);
		destData.put("nodeName", nodeName);
		destData.put("dataType", dataType);
		destData.put("inputNode", inputNode);

		// Data
		SubData data = srcData.getData();
		copyData(data, destData);
		// *********

		int count = srcData.getChildCount();
		if (count > 0) {
			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			destData.put("children", list);
			for (int i = 0; i < count; i++) {
				WSTreeNode node = (WSTreeNode) srcData.getChildAt(i);
				Map<String, Object> child = new LinkedHashMap<String, Object>();
				copyData(node, child, makeKey(key, i));
				list.add(child);
			}
		}
	}

	/**
	 * @param srcData
	 * @param destData
	 */
	private void copyData(SubData srcData, Map<String, Object> destData) {
		// *********
		// Data
		// *********
		String screenField = srcData.getScreenField();
		String definedField = srcData.getDefinedField();
		String integrationField = srcData.getIntegrationField();
		String defaultValue = srcData.getDefaultValue();
		boolean isScreenField = srcData.isScreenField();
		boolean dataList = srcData.isDataList();

		if (isScreenField) {
			destData.put("definedField", screenField);
		} else {
			destData.put("definedField", definedField);
		}
		destData.put("integrationField", integrationField);
		destData.put("defaultValue", defaultValue);
		destData.put("dataList", dataList);
		// *********

		// *********
		// Sub Data
		// *********
		List<SubData> subFields = srcData.getSubFields();
		if (subFields != null) {
			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			for (int i = 0; i < subFields.size(); i++) {
				SubData data = subFields.get(i);
				Map<String, Object> map = new LinkedHashMap<String, Object>();
				copyData(data, map);
				list.add(map);
			}
			destData.put("subFields", list);
		}
		// *********
	}

	/**
	 * @param srcData
	 * @param destData
	 */
	@SuppressWarnings("unchecked")
	private void copyData(Map<String, Object> srcData, JsonNodeData destData) {
		// *********
		// Json Data
		// *********
		String endpointName = (String) srcData.get("endpointName");

		destData.setEndpointName(endpointName);
		// *********

		// *********
		// Fields
		// *********
		Map<String, Object> inputFields = (Map<String, Object>) srcData.get("inputFields");
		Map<String, Object> outputFields = (Map<String, Object>) srcData.get("outputFields");
		WSTreeNode inputRoot = destData.getInputTree(); // Input ROOT
		WSTreeNode outputRoot = destData.getOutputTree(); // output ROOT

		copyData(inputFields, inputRoot);
		copyData(outputFields, outputRoot);
		// *********

	}

	private WSTreeNode makeWSTreeNode(Map<String, Object> srcData) {
		WSTreeNode destData = null;
		String dataType = (String) srcData.get("dataType");
		boolean inputNode = (boolean) srcData.get("inputNode");
		if ("Array".equalsIgnoreCase(dataType)) {
			destData = new WSTreeNode(WSDataTreeType.GROUP, inputNode);
			destData.setDataList(true);
		} else if ("Map".equalsIgnoreCase(dataType)) {
			destData = new WSTreeNode(WSDataTreeType.GROUP, inputNode);
		} else if ("String".equalsIgnoreCase(dataType)) {
			destData = new WSTreeNode(WSDataTreeType.DATA, inputNode);
		} else {
			destData = new WSTreeNode(inputNode);
		}
		return destData;
	}

	/**
	 * @param srcData
	 * @param destData
	 */
	@SuppressWarnings("unchecked")
	private void copyData(Map<String, Object> srcData, WSTreeNode destData) {
		// *********
		// WS Sub Data
		// *********
		// Tree Data
		String nodeName = (String) srcData.get("nodeName");
		destData.setNodeName(nodeName);
		destData.setPromote(true);
		// Data
		SubData data = destData.getData();
		if (data == null) {
			data = new SubData();
			destData.setData(data);
		}
		copyData(srcData, data);
		// *********

		List<Map<String, Object>> list = (List<Map<String, Object>>) srcData.get("children");
		if (list != null) {
			for (int i = 0; i < list.size(); i++) {
				Map<String, Object> child = list.get(i);
				WSTreeNode node = makeWSTreeNode(child);
				copyData(child, node);
				destData.add(node);
			}
		}

	}

	/**
	 * @param srcData
	 * @param destData
	 */
	@SuppressWarnings("unchecked")
	private void copyData(Map<String, Object> srcData, SubData destData) {
		// *********
		// Data
		// *********
		String screenField = (String) srcData.get("screenField");
		String definedField = (String) srcData.get("definedField");
		String integrationField = (String) srcData.get("integrationField");
		String defaultValue = (String) srcData.get("defaultValue");
		boolean isScreenField = false; // (boolean)srcData.get("isScreenField");
		boolean promote = true; // (boolean)srcData.get("promote");
		boolean dataList = (boolean) srcData.get("dataList");
		destData.setIsScreenField(isScreenField);
		if (isScreenField) {
			destData.setScreenField(screenField);
		}
		destData.setDefinedField(definedField);
		destData.setIntegrationField(integrationField);
		destData.setDefaultValue(defaultValue);
		destData.setPromote(promote);
		destData.setIsDataList(dataList);
		// *********

		// *********
		// Sub Data
		// *********
		List<SubData> subFields = null;
		List<Map<String, Object>> list = (List<Map<String, Object>>) srcData.get("subFields");
		if (list != null) {
			subFields = destData.getSubFields();
			if (subFields == null) {
				subFields = new ArrayList<SubData>();
			}
			for (int i = 0; i < list.size(); i++) {
				Map<String, Object> map = list.get(i);
				SubData data = new SubData();
				copyData(map, data);
				subFields.add(data);
			}
		}
		destData.setSubFields(subFields);
		// *********
	}

}
