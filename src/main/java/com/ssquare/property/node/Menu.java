package com.ssquare.property.node;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.ssquare.facade.common.handler.ssquare.data.NodeCell;

import techberry.ewi.graph.editor.EWICell;
import techberry.ewi.graph.editor.MenuData;
import techberry.ewi.graph.editor.SelectionListData;

public class Menu extends Screen {

	@Override
	public void loadProperty(NodeCell nodeCell) throws Exception {

		// call super for screen
		super.loadProperty(nodeCell);

		EWICell cell = nodeCell.cell;
		
		// Update node lable
		updateNodeLabel(cell);
		
		Map<String, Object> props = nodeCell.props;
		MenuData menuData = null;
		if (EWICell.MENU.equals(cell.getCellType())) {
			menuData = cell.getMenuData();
		} else if (EWICell.SELECTIONLIST.equals(cell.getCellType())) {
			menuData = cell.getSelectionListData();
		}

		// Prepare data
		int beginRow = Integer.parseInt(menuData.getBeginRow());
		int endRow = Integer.parseInt(menuData.getEndRow());
		String executeKey = menuData.getExecuteKey();
		String checkType = menuData.getCheckType();
		String nextPageKey = menuData.getNextPageKey();
		int maxPage = menuData.getMaxPage();
		String checkString = menuData.getCheckString();
		int checkRow = Integer.parseInt(menuData.getCheckRow());
		int checkCol = Integer.parseInt(menuData.getCheckCol());
		ArrayList<Map<String, Object>> checkFields = menuData.getCheckFields();
		ArrayList<Map<String, Object>> conditions = menuData.getConditions();

		// Add keys
		addKeys(checkFields);
		addKeys(conditions);

		// set data to result
		props.put("beginRow", beginRow);
		props.put("endRow", endRow);
		props.put("executeKey", executeKey);
		props.put("checkType", checkType);
		props.put("nextPageKey", nextPageKey);
		props.put("maxPage", maxPage);
		props.put("checkString", checkString);
		props.put("checkRow", checkRow);
		props.put("checkCol", checkCol);
		props.put("checkFields", checkFields);
		props.put("conditions", conditions);

	}

	/**
	 * add keys
	 * 
	 * @param fields
	 */
	private void addKeys(List<Map<String, Object>> fields) {
		if (fields != null) {
			for (Map<String, Object> map : fields) {
				String key = UUID.randomUUID().toString();
				map.put("key", key);
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void saveProperty(NodeCell nodeCell) throws Exception {

		// call super for screen
		super.saveProperty(nodeCell);

		EWICell cell = nodeCell.cell;
		Map<String, Object> props = nodeCell.props;

		// Prepare data
		int beginRow = (int) props.get("beginRow");
		int endRow = (int) props.get("endRow");
		String executeKey = (String) props.get("executeKey");
		String checkType = (String) props.get("checkType");
		String nextPageKey = (String) props.get("nextPageKey");
		int maxPage = (int) props.get("maxPage");
		String checkString = (String) props.get("checkString");
		int checkRow = (int) props.get("checkRow");
		int checkCol = (int) props.get("checkCol");
		ArrayList<Map<String, Object>> checkFields = (ArrayList<Map<String, Object>>) props.get("checkFields");
		ArrayList<Map<String, Object>> conditions = (ArrayList<Map<String, Object>>) props.get("conditions");

		// Init menu data
		MenuData menuData = null;
		if (EWICell.MENU.equals(cell.getCellType())) {
			menuData = new MenuData();
			cell.setMenuData(menuData);
		} else if (EWICell.SELECTIONLIST.equals(cell.getCellType())) {
			SelectionListData data = new SelectionListData();
			cell.setSelectionListData(data);
			menuData = data;
		}

		// put data back to cell
		menuData.setBeginRow(beginRow + "");
		menuData.setEndRow(endRow + "");
		menuData.setExecuteKey(executeKey);
		menuData.setCheckType(checkType);
		menuData.setNextPageKey(nextPageKey);
		menuData.setMaxPage(maxPage);
		menuData.setCheckString(checkString);
		menuData.setCheckRow(checkRow + "");
		menuData.setCheckCol(checkCol + "");

		// loop to add check field
		if (checkFields != null) {
			for (Map<String, Object> field : checkFields) {

				String fieldName = (String) field.get("fieldName");
				int row = (int) field.get("row");
				int col = (int) field.get("col");
				int length = (int) field.get("length");
				String marker = (String) field.get("marker");
				int markerRow = (int) field.get("markerRow");
				int markerCol = (int) field.get("markerCol");

				// Add data
				menuData.addCheckField(fieldName, row, col, length, marker, markerRow, markerCol);

			}
		}

		// loop to add condition
		if (conditions != null) {
			for (Map<String, Object> field : conditions) {

				String combineCondition = (String) field.get("combineCondition");
				String inputFieldName = (String) field.get("inputFieldName");
				String operator = (String) field.get("operator");
				String checkFieldName = (String) field.get("checkFieldName");

				// Add data
				menuData.addCondition(combineCondition, inputFieldName, operator, checkFieldName);

			}
		}

		// Update node lable
		updateNodeLabel(cell);

	}
}
