package com.ssquare.property.node;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.ssquare.consts.DataTypeConsts;
import com.ssquare.facade.common.handler.ssquare.Flow;
import com.ssquare.facade.common.handler.ssquare.data.NodeCell;
import com.ssquare.property.NodeProps;
import com.ssquare.util.RootDataUtil;

import techberry.ewi.graph.editor.EWICell;
import techberry.ewi.graph.editor.SVData;

public class SubService extends NodeProps {

	@Override
	public void loadProperty(NodeCell nodeCell) throws Exception {

		EWICell cell = nodeCell.cell;
		
		// Update node lable
		updateNodeLabel(cell);
		
		
		Map<String, Object> props = nodeCell.props;
		SVData svData = cell.getSVData();

		// Prepare data
		String serviceName = svData.getServiceName();
		int loopType = svData.getLoopType();
		String loopListName = svData.getLoopListName();
		String loopAmount = svData.getLoopAmount();
		String loopCondition = svData.getLoopCondition();

		// Put data to result
		props.put("serviceName", serviceName);
		props.put("loopType", loopType);
		props.put("loopListName", loopListName);
		props.put("loopAmount", loopAmount);
		props.put("loopCondition", loopCondition);

	}

	@Override
	public void saveProperty(NodeCell nodeCell) throws Exception {

		EWICell cell = nodeCell.cell;
		Map<String, Object> props = nodeCell.props;

		// Prepare data
		String serviceName = (String) props.get("serviceName");
		int loopType = (int) props.get("loopType");
		String loopListName = (String) props.get("loopListName");
		String loopAmount = (String) props.get("loopAmount");
		String loopCondition = (String) props.get("loopCondition");

		// Initial SvData
		SVData svData = cell.getSVData();
		if (svData == null) {
			svData = new SVData();
			cell.setSVData(svData);
		}

		// Put data to cell
		svData.setServiceName(serviceName);
		svData.setLoopType(loopType);
		svData.setLoopListName(loopListName);
		svData.setLoopAmount(loopAmount);
		svData.setLoopCondition(loopCondition);

		// Update node lable
		updateNodeLabel(cell);

	}

	@Override
	public Map<String, Object> listDataListName(NodeCell nodeCell) {
		Map<String, Object> fields = new LinkedHashMap<String, Object>();
		String serviceName = (String) nodeCell.props.get("serviceName");
		// To get flow
		Flow flow = common.ssquare.getFlow(serviceName);
		List<Map<String, Object>> list = flow.getPropmoteModel();
		if (list != null) {
			for (Map<String, Object> field : list) {
				String path = (String) field.get("path");
				if (path != null && path.startsWith("ROOT.content.") && "ARRAY".equalsIgnoreCase((String) field.get("type"))) {
					String name = getNameByPath(path);
					boolean promoteIn = getBooleanValue(field.get("promote_input"), true);
					boolean promoteOut = getBooleanValue(field.get("promote_output"), true);
					Map<String, Object> map = RootDataUtil.makeField(name, path, "Array", DataTypeConsts.CREATE_TYPE_AUTO, "", promoteIn, promoteOut);
					fields.put(name, map);
				}
			}
		}
		return fields;
	}

	@Override
	public Map<String, Object> listFieldName(NodeCell nodeCell) {
		Map<String, Object> fields = new LinkedHashMap<String, Object>();
		String serviceName = (String) nodeCell.props.get("serviceName");
		// To get flow
		Flow flow = common.ssquare.getFlow(serviceName);
		List<Map<String, Object>> list = flow.getPropmoteModel();
		if (list != null) {
			String dataListPath = "<No>";
			for (Map<String, Object> field : list) {
				String path = (String) field.get("path");
				if (path != null && path.startsWith("ROOT.content.")) {
					if (path.contains(dataListPath)) {
						// sub field
					} else if (!"ARRAY".equalsIgnoreCase((String) field.get("type"))) {
						String name = getNameByPath(path);
						boolean promoteIn = getBooleanValue(field.get("promote_input"), true);
						boolean promoteOut = getBooleanValue(field.get("promote_output"), true);
						Map<String, Object> map = RootDataUtil.makeField(name, path, "String", DataTypeConsts.CREATE_TYPE_AUTO, "", promoteIn, promoteOut);
						fields.put(name, map);
					} else {
						dataListPath = path + ".";
					}
				}
			}
		}
		return fields;
	}

	@Override
	public Map<String, Object> listFieldNameInDataList(NodeCell nodeCell, String listName) {
		Map<String, Object> fields = new LinkedHashMap<String, Object>();
		String serviceName = (String) nodeCell.props.get("serviceName");
		// To get flow
		Flow flow = common.ssquare.getFlow(serviceName);
		List<Map<String, Object>> list = flow.getPropmoteModel();
		if (list != null) {
			listName = "." + listName + ".";
			for (Map<String, Object> field : list) {
				String path = (String) field.get("path");
				if (path != null && path.startsWith("ROOT.content.") && path.indexOf(listName) >= 0 && !"ARRAY".equalsIgnoreCase((String) field.get("type"))) {
					String name = getNameByPath(path);
					boolean promoteIn = getBooleanValue(field.get("promote_input"), true);
					boolean promoteOut = getBooleanValue(field.get("promote_output"), true);
					Map<String, Object> map = RootDataUtil.makeField(name, path, "String", DataTypeConsts.CREATE_TYPE_AUTO, "", promoteIn, promoteOut);
					fields.put(name, map);
				}
			}
		}
		return fields;
	}

	private String getNameByPath(String path) {
		int pos = -1;
		if (path != null && (pos = path.lastIndexOf(".")) >= 0) {
			return path.substring(pos + 1);
		}
		return path;
	}

	private boolean getBooleanValue(Object obj, boolean defaultValue) {
		if (obj == null) {
			return defaultValue;
		} else if (obj instanceof String) {
			return "ture".equalsIgnoreCase((String) obj);
		} else if (obj instanceof Boolean) {
			return (boolean) obj;
		} else {
			return defaultValue;
		}
	}

}
