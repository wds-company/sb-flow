package com.ssquare.property.node;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.ssquare.facade.common.handler.ssquare.data.NodeCell;
import com.ssquare.property.NodeProps;

import techberry.ewi.graph.editor.EWICell;
import techberry.ewi.graph.editor.SubData;
import techberry.ewi.graph.editor.WSData;
import techberry.ewi.graph.editor.WSDataTreeType;
import techberry.ewi.graph.editor.WSTreeNode;

public class WebService extends NodeProps {

	public static String TYPE_GROUP = "GROUP";
	public static String TYPE_DATA = "DATA";

	@Override
	public void loadProperty(NodeCell nodeCell) throws Exception {

		Map<String, Object> props = nodeCell.props;
		EWICell cell = nodeCell.cell;
		WSData data = cell.getWSData();

		// Update node lable
		updateNodeLabel(cell);

		// Cell -> Props
		copyData(data, props);

	}

	@Override
	public void saveProperty(NodeCell nodeCell) throws Exception {

		Map<String, Object> props = nodeCell.props;
		EWICell cell = nodeCell.cell;
		WSData data = cell.getWSData();
		if (data == null) {
			data = new WSData(true);
			cell.setWSData(data);
		}
		// Props --> Cell
		copyData(props, data);

		// Update node lable
		updateNodeLabel(cell);

	}

	/**
	 * To merge data from WSDL and return map data
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object mergePlugConfig(NodeCell nodeCell, Object objMapProps) throws Exception {

		// Old data in property
		Map<String, Object> oldNodeProps = (Map<String, Object>) objMapProps;
		String portType = (String) oldNodeProps.get("portType");
		String operation = (String) oldNodeProps.get("operation");
		String endpointName = (String) oldNodeProps.get("endpointName");

		// Data
		EWICell cell = nodeCell.cell;

		// Call to refresh input/output
		common.ssquare.webservice.refreshInputOutputFromWSDL(cell, endpointName, portType, operation);

		// load property
		loadProperty(nodeCell);

		Map<String, Object> newNodeProps = nodeCell.props;

		// Set value from oldNodeProps
		newNodeProps.put("endpointName", oldNodeProps.get("endpointName"));
		newNodeProps.put("portType", oldNodeProps.get("portType"));
		newNodeProps.put("operation", oldNodeProps.get("operation"));

		Map<String, Object> oldRootField = new LinkedHashMap<String, Object>();
		Map<String, Object> newRootField = new LinkedHashMap<String, Object>();
		oldRootField.put("nodeName", "ROOT");
		newRootField.put("nodeName", "ROOT");

		// Merge Input
		oldRootField.put("children", oldNodeProps.get("inputFields"));
		newRootField.put("children", newNodeProps.get("inputFields"));
		doMergeMap(oldRootField, newRootField);

		// Merge Output
		oldRootField.put("children", oldNodeProps.get("outputFields"));
		newRootField.put("children", newNodeProps.get("outputFields"));
		doMergeMap(oldRootField, newRootField);

		return newNodeProps;
	}

	/**
	 * Merge new map with old map
	 * 
	 * @param oldMap
	 * @param newMap
	 */
	private void doMergeMap(Map<String, Object> oldMap, Map<String, Object> newMap) {

		// Call Merge Data
		// Input / Output
		// If path is corrected at newNodeProps, set value from oldNodeProps
		if (oldMap != null && newMap != null) {
			copyFieldData(oldMap, newMap);
		}

	}

	/**
	 * @param srcData
	 * @param destData
	 */
	private void copyData(WSData srcData, Map<String, Object> destData) {
		// *********
		// WS Data
		// *********
		String endpointName = srcData.getEnpointName();
		String operation = srcData.getOperation();
		String portType = srcData.getPortType();
		String key = makeKey(null, "$");

		destData.put("endpointName", endpointName);
		destData.put("operation", operation);
		destData.put("portType", portType);
		destData.put("key", key);
		// *********

		// *********
		// Fields
		// *********
		List<Map<String, Object>> inputFields = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> outputFields = new ArrayList<Map<String, Object>>();
		WSTreeNode inputTree = srcData.getInputTree();
		WSTreeNode outputTree = srcData.getOutputTree();
		copyData(inputTree, inputFields, makeKey(key, "inputFields"));
		copyData(outputTree, outputFields, makeKey(key, "outputFields"));
		destData.put("inputFields", inputFields);
		destData.put("outputFields", outputFields);
		// *********

		// *********
		// WS Async
		// *********
		/*
		 * boolean asyncMode = srcData.isAsyncMode(); String serviceCallback =
		 * srcData.getServiceCallback(); String endpointCallback =
		 * srcData.getEndpointCallback(); String messageId = srcData.getMessageId();
		 * 
		 * destData.put("asyncMode", asyncMode); destData.put("serviceCallback",
		 * serviceCallback); destData.put("endpointCallback", endpointCallback);
		 * destData.put("messageId", messageId);
		 */
		// *********

		// *********
		// all operation from wsdl
		String[] operations = srcData.getOperations();
		String[] portTypes = srcData.getPortTypes();

		destData.put("operations", operations);
		destData.put("portTypes", portTypes);
		// *********

	}

	/**
	 * @param srcData
	 * @param destData
	 * @param parentKey
	 */
	private void copyData(WSTreeNode srcData, Map<String, Object> destData, String parentKey) {
		// *********
		// WS Sub Data
		// *********
		// Tree Data
		String nodeType = srcData.getNodeType().name();
		String nodeName = srcData.getNodeName();
		String key = makeKey(parentKey, nodeName);
		boolean dataList = srcData.isDataList();
		boolean inputNode = srcData.isInputNode();
		String fieldType = "String";
		if (dataList) {
			fieldType = "Array";
		} else if (TYPE_GROUP.equalsIgnoreCase(nodeType)) {
			fieldType = "Map";
		}

		destData.put("key", key);
		destData.put("nodeType", nodeType);
		destData.put("nodeName", nodeName);
		destData.put("fieldType", fieldType);
		destData.put("inputNode", inputNode);

		// Data
		SubData data = srcData.getData();
		copyData(data, destData);

		if (!"String".equals(fieldType)) {
			// List
			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			copyData(srcData, list, key);
			destData.put("children", list);
		}
		// *********
	}

	private void copyData(WSTreeNode srcData, List<Map<String, Object>> list, String parentKey) {
		int count = srcData.getChildCount();
		if (count > 0) {
			for (int i = 0; i < count; i++) {
				WSTreeNode node = (WSTreeNode) srcData.getChildAt(i);
				Map<String, Object> child = new LinkedHashMap<String, Object>();
				copyData(node, child, makeKey(parentKey, i));
				list.add(child);
			}
		}
	}

	/**
	 * @param srcData
	 * @param destData
	 */
	private void copyData(SubData srcData, Map<String, Object> destData) {
		// *********
		// Data
		// *********
		String screenField = srcData.getScreenField();
		String definedField = srcData.getDefinedField();
		String integrationField = srcData.getIntegrationField();
		String defaultValue = srcData.getDefaultValue();
		boolean isScreenField = srcData.isScreenField();
		boolean dataList = srcData.isDataList();

		if (isScreenField) {
			destData.put("definedField", screenField);
		} else {
			destData.put("definedField", definedField);
		}
		destData.put("integrationField", integrationField);
		destData.put("defaultValue", defaultValue);
		destData.put("dataList", dataList);
		// *********

		// *********
		// Sub Data
		// *********
		List<SubData> subFields = srcData.getSubFields();
		if (subFields != null) {
			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			for (int i = 0; i < subFields.size(); i++) {
				SubData data = subFields.get(i);
				Map<String, Object> map = new LinkedHashMap<String, Object>();
				copyData(data, map);
				list.add(map);
			}
			destData.put("subFields", list);
		}
		// *********
	}

	/**
	 * @param srcData
	 * @param destData
	 */
	private void copyData(Map<String, Object> srcData, WSData destData) {
		// *********
		// WS Data
		// *********
		String endpointName = (String) srcData.get("endpointName");
		String operation = (String) srcData.get("operation");
		String portType = (String) srcData.get("portType");

		destData.setEnpoint(endpointName);
		destData.setOperation(operation);
		destData.setPortType(portType);
		// *********

		// *********
		// Fields
		// *********
		Map<String, Object> inputFields = new LinkedHashMap<String, Object>();
		Map<String, Object> outputFields = new LinkedHashMap<String, Object>();
		inputFields.put("nodeName", "ROOT");
		// inputFields.put("dataList", false);
		outputFields.put("nodeName", "ROOT");
		// outputFields.put("dataList", false);
		inputFields.put("children", srcData.get("inputFields"));
		outputFields.put("children", srcData.get("outputFields"));
		WSTreeNode inputRoot = destData.getInputTree(); // Input ROOT
		WSTreeNode outputRoot = destData.getOutputTree(); // output ROOT

		copyData(inputFields, inputRoot);
		copyData(outputFields, outputRoot);
		// *********

		// *********
		// WS Async
		// *********
		/*
		 * boolean asyncMode = (boolean)srcData.get("asyncMode"); String serviceCallback
		 * = (String)srcData.get("serviceCallback"); String endpointCallback =
		 * (String)srcData.get("endpointCallback"); String messageId =
		 * (String)srcData.get("messageId");
		 * 
		 * destData.setAsyncMode(asyncMode);
		 * destData.setServiceCallback(serviceCallback);
		 * destData.setEndpointCallback(endpointCallback);
		 * destData.setMessageId(messageId);
		 */
		// *********

		// *********
		// all operation from wsdl
		String[] operations = getArrayString(srcData.get("operations"));
		String[] portTypes = getArrayString(srcData.get("portTypes"));

		destData.setOperations(operations);
		destData.setPortTypes(portTypes);
		// *********
	}

	/**
	 * @param srcData
	 * @return WSTreeNode
	 */
	private WSTreeNode makeWSTreeNode(Map<String, Object> srcData) {
		WSTreeNode destData = null;
		String fieldType = (String) srcData.get("fieldType");
		boolean inputNode = (boolean) srcData.get("inputNode");
		if ("Array".equalsIgnoreCase(fieldType)) {
			destData = new WSTreeNode(WSDataTreeType.GROUP, inputNode);
			destData.setDataList(true);
		} else if ("Map".equalsIgnoreCase(fieldType)) {
			destData = new WSTreeNode(WSDataTreeType.GROUP, inputNode);
		} else if ("String".equalsIgnoreCase(fieldType)) {
			destData = new WSTreeNode(WSDataTreeType.DATA, inputNode);
		} else {
			destData = new WSTreeNode(inputNode);
		}
		return destData;
	}

	/**
	 * @param srcData
	 * @param destData
	 */
	@SuppressWarnings("unchecked")
	private void copyData(Map<String, Object> srcData, WSTreeNode destData) {
		// *********
		// WS Sub Data
		// *********
		// Tree Data
		String nodeName = (String) srcData.get("nodeName");
		destData.setNodeName(nodeName);
		destData.setPromote(true);
		// Data
		SubData data = destData.getData();
		if (data == null) {
			data = new SubData();
			destData.setData(data);
		}
		copyData(srcData, data);
		// *********

		List<Map<String, Object>> list = (List<Map<String, Object>>) srcData.get("children");
		if (list != null) {
			for (int i = 0; i < list.size(); i++) {
				Map<String, Object> child = list.get(i);
				WSTreeNode node = makeWSTreeNode(child);
				copyData(child, node);
				destData.add(node);
			}
		}

	}

	/**
	 * @param srcData
	 * @param destData
	 */
	@SuppressWarnings("unchecked")
	private void copyData(Map<String, Object> srcData, SubData destData) {
		// *********
		// Data
		// *********
		String screenField = (String) srcData.get("screenField");
		String definedField = (String) srcData.get("definedField");
		String integrationField = (String) srcData.get("integrationField");
		String defaultValue = (String) srcData.get("defaultValue");
		boolean isScreenField = false; // (boolean)srcData.get("isScreenField");
		boolean promote = true; // (boolean)srcData.get("promote");
		boolean dataList = getBooleanValue(srcData.get("dataList"));
		destData.setIsScreenField(isScreenField);
		if (isScreenField) {
			destData.setScreenField(screenField);
		}
		destData.setDefinedField(definedField);
		destData.setIntegrationField(integrationField);
		destData.setDefaultValue(defaultValue);
		destData.setPromote(promote);
		destData.setIsDataList(dataList);
		// *********

		// *********
		// Sub Data
		// *********
		List<SubData> subFields = null;
		List<Map<String, Object>> list = (List<Map<String, Object>>) srcData.get("subFields");
		if (list != null) {
			subFields = destData.getSubFields();
			if (subFields == null) {
				subFields = new ArrayList<SubData>();
			}
			for (int i = 0; i < list.size(); i++) {
				Map<String, Object> map = list.get(i);
				SubData data = new SubData();
				copyData(map, data);
				subFields.add(data);
			}
		}
		destData.setSubFields(subFields);
		// *********
	}

	/**
	 * copy field
	 * 
	 * @param srcData
	 * @param destData
	 */
	@SuppressWarnings("unchecked")
	private void copyFieldData(Map<String, Object> srcData, Map<String, Object> destData) {
		String srcNodeName = (String) srcData.get("nodeName");
		String destNodeName = (String) destData.get("nodeName");
		if (srcNodeName != null && destNodeName != null && destNodeName.equalsIgnoreCase(srcNodeName)) {

			// *********
			// Data
			// *********
			String definedField = (String) srcData.get("definedField");
			if (definedField != null) {
				String integrationField = (String) srcData.get("integrationField");
				String defaultValue = (String) srcData.get("defaultValue");
				boolean dataList = getBooleanValue(srcData.get("dataList"));

				destData.put("definedField", definedField);
				destData.put("integrationField", integrationField);
				destData.put("defaultValue", defaultValue);
				destData.put("dataList", dataList);
			}
			// *********

			List<Map<String, Object>> srcList = (List<Map<String, Object>>) srcData.get("children");
			List<Map<String, Object>> destList = (List<Map<String, Object>>) destData.get("children");
			if (srcList != null && destList != null) {
				for (int i = 0; i < destList.size(); i++) {
					Map<String, Object> child = destList.get(i);
					String nodeName = (String) child.get("nodeName");
					if (nodeName != null) {
						for (int j = 0; j < srcList.size(); j++) {
							Map<String, Object> tmp = srcList.get(j);
							if (nodeName.equalsIgnoreCase((String) tmp.get("nodeName"))) {
								copyFieldData(tmp, child);
								break;
							}
						}
					}
				}
			}

		}
	}

	private boolean getBooleanValue(Object obj) {
		if (obj == null) {
			return false;
		} else if (obj instanceof String) {
			return "ture".equalsIgnoreCase((String) obj);
		} else if (obj instanceof Boolean) {
			return (boolean) obj;
		} else {
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	private String[] getArrayString(Object obj) {
		if (obj == null) {
			return null;
		}
		if (obj instanceof List) {
			return ((List<String>) obj).toArray(new String[0]);
		} else if (obj instanceof String) {
			return new String[] { (String) obj };
		} else if (obj instanceof String[]) {
			return (String[]) obj;
		} else {
			return null;
		}
	}

	@Override
	public Map<String, Object> listDataListName(NodeCell nodeCell) {

		// Property from font-end
		Map<String, Object> props = nodeCell.props;

		// Find list name
		Map<String, Object> data1 = findField(props, "inputFields", "definedField", "Map", "dataList", true, true, false);
		Map<String, Object> data2 = findField(props, "outputFields", "definedField", "Map", "dataList", true, false, true);
		data1.putAll(data2);

		return data1;
	}

	@Override
	public Map<String, Object> listFieldName(NodeCell nodeCell) {

		// Property from font-end
		Map<String, Object> props = nodeCell.props;

		// Find list name
		Map<String, Object> data1 = findField(props, "inputFields", "definedField", "Map", "dataList", false, true, false);
		Map<String, Object> data2 = findField(props, "outputFields", "definedField", "Map", "dataList", false, false, true);
		data1.putAll(data2);

		return data1;
	}

	@Override
	public Map<String, Object> listFieldNameInDataList(NodeCell nodeCell, String listName) {

		// Property from font-end
		Map<String, Object> props = nodeCell.props;

		// Find list name
		Map<String, Object> data1 = findFieldInList(props, "inputFields", "definedField", "Map", "dataList", listName, true, false);
		Map<String, Object> data2 = findFieldInList(props, "outputFields", "definedField", "Map", "dataList", listName, false, true);
		data1.putAll(data2);

		return data1;
	}

}
