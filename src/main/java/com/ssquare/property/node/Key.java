package com.ssquare.property.node;

import java.util.Map;

import com.ssquare.facade.common.handler.ssquare.data.NodeCell;
import com.ssquare.property.NodeProps;

import techberry.ewi.graph.editor.EWICell;

public class Key extends NodeProps {

	@Override
	public void loadProperty(NodeCell nodeCell) throws Exception {
		
		EWICell cell = nodeCell.cell;
		Map<String, Object> props = nodeCell.props;
		
		String codeString = cell.getCodeString();
		
		String key = common.ssquare.screenKey.getKeyFromExecuteCode(codeString);
		
		// Set key
		props.put("codeString", key);
	}

	@Override
	public void saveProperty(NodeCell nodeCell) throws Exception {

		EWICell cell = nodeCell.cell;
		Map<String, Object> props = nodeCell.props;
		
		String key = (String) props.get("codeString");
		
		String codeString = common.ssquare.screenKey.getKeyExecuteCode(key);
		
		// Set execute key
		cell.setCodeString(codeString);
		
	}

}
