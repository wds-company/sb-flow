package com.ssquare.property.node;

import com.ssquare.facade.common.handler.ssquare.data.NodeCell;
import com.ssquare.property.NodeProps;

public class Code extends NodeProps {

	@Override
	public void loadProperty(NodeCell nodeCell) throws Exception {
		// Update node lable
		updateNodeLabel(nodeCell.cell);
	}

	@Override
	public void saveProperty(NodeCell nodeCell) throws Exception {
		// Update node lable
		updateNodeLabel(nodeCell.cell);
	}

}
