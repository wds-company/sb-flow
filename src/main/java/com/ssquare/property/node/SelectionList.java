package com.ssquare.property.node;

import java.util.Map;

import com.ssquare.facade.common.handler.ssquare.data.NodeCell;

import techberry.ewi.data.bean.entity.FieldBean;
import techberry.ewi.graph.editor.EWICell;
import techberry.ewi.graph.editor.SelectionListData;

public class SelectionList extends Menu {

	@Override
	public void loadProperty(NodeCell nodeCell) throws Exception {

		// Data
		EWICell cell = nodeCell.cell;

		// Update node lable
		updateNodeLabel(cell);

		Map<String, Object> props = nodeCell.props;
		SelectionListData slData = cell.getSelectionListData();

		// Make default data
		slData.setBeginRow(0 + "");
		slData.setEndRow(0 + "");
		slData.setCheckRow(0 + "");
		slData.setCheckCol(0 + "");

		// Call super class
		super.loadProperty(nodeCell);

		// Prepare data
		boolean disableExecuteKey = slData.isDisableExecuteKey();
		boolean callSubService = slData.isCallSubService();
		boolean callSubServiceIfNotMatch = slData.isCallSubServiceIfNotMatch();
		String serviceName = slData.getServiceName();
		String serviceNameIfNotMatch = slData.getServiceNameIfNotMatch();
		String matchingType = slData.getMatchingType();

		String inputListName = null;
		FieldBean bean = slData.getListField();
		if (bean != null) {
			inputListName = bean.getName();
		}

		// Put data to result
		props.put("disableExecuteKey", disableExecuteKey);
		props.put("callSubService", callSubService);
		props.put("callSubServiceIfNotMatch", callSubServiceIfNotMatch);
		props.put("serviceName", serviceName);
		props.put("serviceNameIfNotMatch", serviceNameIfNotMatch);
		props.put("matchingType", matchingType);
		props.put("inputListName", inputListName);

	}

	@Override
	public void saveProperty(NodeCell nodeCell) throws Exception {

		// Data
		EWICell cell = nodeCell.cell;
		Map<String, Object> props = nodeCell.props;

		// Make default data
		props.put("beginRow", 0);
		props.put("endRow", 0);
		props.put("checkRow", 0);
		props.put("checkCol", 0);

		// Call super class
		super.saveProperty(nodeCell);

		// Prepare data
		boolean disableExecuteKey = (boolean) props.get("disableExecuteKey");
		boolean callSubService = (boolean) props.get("callSubService");
		boolean callSubServiceIfNotMatch = (boolean) props.get("callSubServiceIfNotMatch");
		String serviceName = (String) props.get("serviceName");
		String serviceNameIfNotMatch = (String) props.get("serviceNameIfNotMatch");
		String matchingType = (String) props.get("matchingType");
		String inputListName = (String) props.get("inputListName");
		FieldBean bean = null;

		// Find bean data
		if (inputListName != null) {
			bean = common.ssquare.screen.getFieldByScreen(cell.getScreen(), inputListName);
		}

		// Put data to cell
		SelectionListData slData = cell.getSelectionListData();
		slData.setDisableExecuteKey(disableExecuteKey);
		slData.setCallSubService(callSubService);
		slData.setCallSubServiceIfNotMatch(callSubServiceIfNotMatch);
		slData.setServiceName(serviceName);
		slData.setServiceNameIfNotMatch(serviceNameIfNotMatch);
		slData.setMatchingType(matchingType);
		slData.setListField(bean);

		// Update node lable
		updateNodeLabel(cell);

	}

}
