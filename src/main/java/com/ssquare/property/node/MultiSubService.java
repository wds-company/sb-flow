package com.ssquare.property.node;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.ssquare.consts.DataTypeConsts;
import com.ssquare.facade.common.handler.ssquare.Flow;
import com.ssquare.facade.common.handler.ssquare.data.NodeCell;
import com.ssquare.property.NodeProps;
import com.ssquare.util.RootDataUtil;

import techberry.ewi.graph.editor.EWICell;
import techberry.ewi.graph.editor.prop.data.MSVData;

public class MultiSubService extends NodeProps {

	@Override
	public void loadProperty(NodeCell nodeCell) throws Exception {

		EWICell cell = nodeCell.cell;
		Map<String, Object> props = nodeCell.props;
		MSVData msvData = cell.getMSVData();
		
		// Update node lable
		updateNodeLabel(cell);

		// Prepare data
		int numberOfSubService = msvData.getNumberOfSubService();
		int serviceTimeout = msvData.getServiceTimeout();
		List<Map<String, Object>> serviceNames = new ArrayList<Map<String, Object>>();

		// Put data to result
		props.put("numberOfSubService", numberOfSubService);
		props.put("serviceTimeout", serviceTimeout);
		props.put("serviceNames", serviceNames);

		// Loop to add sub service name
		for (int i = 0; i < numberOfSubService; i++) {
			Map<String, Object> map = new HashMap<String, Object>();

			boolean ignoreError = msvData.isIgnoreError(i);
			String serviceName = msvData.getServiceName(i);

			map.put("ignoreError", ignoreError);
			map.put("serviceName", serviceName);

			serviceNames.add(map);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void saveProperty(NodeCell nodeCell) throws Exception {

		EWICell cell = nodeCell.cell;
		Map<String, Object> props = nodeCell.props;

		// Prepare data
		int numberOfSubService = (int) props.get("numberOfSubService");
		int serviceTimeout = (int) props.get("serviceTimeout");
		List<Map<String, Object>> serviceNames = (List<Map<String, Object>>) props.get("serviceNames");

		// initial data
		MSVData msvData = cell.getMSVData();
		if (msvData == null) {
			msvData = new MSVData();
			cell.setMSVData(msvData);
		}

		// Put data to cell
		msvData.setNumberOfSubService(numberOfSubService);
		msvData.setServiceTimeout(serviceTimeout);

		if (serviceNames != null) {
			for (int i = 0; i < serviceNames.size(); i++) {
				Map<String, Object> map = serviceNames.get(i);

				boolean ignoreError = (boolean) map.get("ignoreError");
				String name = (String) map.get("serviceName");

				msvData.setIgnoreError(i, ignoreError);
				msvData.setServiceName(i, name);
			}
		}

		// Update node lable
		updateNodeLabel(cell);

	}

	@Override
	public Map<String, Object> listDataListName(NodeCell nodeCell) {
		Map<String, Object> fields = new LinkedHashMap<String, Object>();
		EWICell cell = nodeCell.cell;
		MSVData msvData = cell.getMSVData();
		int numberOfSubService = msvData.getNumberOfSubService();
		// Loop to add sub service name
		for (int i = 0; i < numberOfSubService; i++) {
			String serviceName = msvData.getServiceName(i);
			// To get flow
			Flow flow = common.ssquare.getFlow(serviceName);
			List<Map<String, Object>> list = flow.getPropmoteModel();
			if (list != null) {
				for (Map<String, Object> field : list) {
					String path = (String) field.get("path");
					if (path != null && path.startsWith("ROOT.content.") && "ARRAY".equalsIgnoreCase((String) field.get("type"))) {
						String name = getNameByPath(path);
						boolean promoteIn = getBooleanValue(field.get("promote_input"), true);
						boolean promoteOut = getBooleanValue(field.get("promote_output"), true);
						Map<String, Object> map = RootDataUtil.makeField(name, path, "Array", DataTypeConsts.CREATE_TYPE_AUTO, "", promoteIn, promoteOut);
						fields.put(name, map);
					}
				}
			}
		}
		return fields;
	}

	@Override
	public Map<String, Object> listFieldName(NodeCell nodeCell) {
		Map<String, Object> fields = new LinkedHashMap<String, Object>();
		EWICell cell = nodeCell.cell;
		MSVData msvData = cell.getMSVData();
		int numberOfSubService = msvData.getNumberOfSubService();
		// Loop to add sub service name
		for (int i = 0; i < numberOfSubService; i++) {
			String serviceName = msvData.getServiceName(i);
			// To get flow
			Flow flow = common.ssquare.getFlow(serviceName);
			List<Map<String, Object>> list = flow.getPropmoteModel();
			if (list != null) {
				String dataListPath = "<No>";
				for (Map<String, Object> field : list) {
					String path = (String) field.get("path");
					if (path != null && path.startsWith("ROOT.content.")) {
						if (path.contains(dataListPath)) {
							// sub field
						} else if (!"ARRAY".equalsIgnoreCase((String) field.get("type"))) {
							String name = getNameByPath(path);
							boolean promoteIn = getBooleanValue(field.get("promote_input"), true);
							boolean promoteOut = getBooleanValue(field.get("promote_output"), true);
							Map<String, Object> map = RootDataUtil.makeField(name, path, "String", DataTypeConsts.CREATE_TYPE_AUTO, "", promoteIn, promoteOut);
							fields.put(name, map);
						} else {
							dataListPath = path + ".";
						}
					}
				}
			}
		}
		return fields;
	}

	@Override
	public Map<String, Object> listFieldNameInDataList(NodeCell nodeCell, String listName) {
		Map<String, Object> fields = new LinkedHashMap<String, Object>();
		EWICell cell = nodeCell.cell;
		MSVData msvData = cell.getMSVData();
		int numberOfSubService = msvData.getNumberOfSubService();
		// Loop to add sub service name
		for (int i = 0; i < numberOfSubService; i++) {
			String serviceName = msvData.getServiceName(i);
			// To get flow
			Flow flow = common.ssquare.getFlow(serviceName);
			List<Map<String, Object>> list = flow.getPropmoteModel();
			if (list != null) {
				listName = "." + listName + ".";
				for (Map<String, Object> field : list) {
					String path = (String) field.get("path");
					if (path != null && path.startsWith("ROOT.content.") && path.indexOf(listName) >= 0 && !"ARRAY".equalsIgnoreCase((String) field.get("type"))) {
						String name = getNameByPath(path);
						boolean promoteIn = getBooleanValue(field.get("promote_input"), true);
						boolean promoteOut = getBooleanValue(field.get("promote_output"), true);
						Map<String, Object> map = RootDataUtil.makeField(name, path, "String", DataTypeConsts.CREATE_TYPE_AUTO, "", promoteIn, promoteOut);
						fields.put(name, map);
					}
				}
			}
		}
		return fields;
	}

	private String getNameByPath(String path) {
		int pos = -1;
		if (path != null && (pos = path.lastIndexOf(".")) >= 0) {
			return path.substring(pos + 1);
		}
		return path;
	}

	private boolean getBooleanValue(Object obj, boolean defaultValue) {
		if (obj == null) {
			return defaultValue;
		} else if (obj instanceof String) {
			return "ture".equalsIgnoreCase((String) obj);
		} else if (obj instanceof Boolean) {
			return (boolean) obj;
		} else {
			return defaultValue;
		}
	}

}
