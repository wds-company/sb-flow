package com.ssquare.property.node;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.ssquare.facade.common.handler.ssquare.data.NodeCell;
import com.ssquare.property.NodeProps;

import techberry.ewi.graph.editor.EWICell;
import techberry.ewi.graph.editor.PCData;
import techberry.ewi.programcall.data.EWIAS400DataType;

public class ProgramCall extends NodeProps {

	private static final String ROOT_NODE = "PARAMS";

	@Override
	public void loadProperty(NodeCell nodeCell) throws Exception {

		Map<String, Object> props = nodeCell.props;
		EWICell cell = nodeCell.cell;
		
		// Update node lable
		updateNodeLabel(cell);
		
		PCData data = cell.getPCData();
		if (data == null) {
			data = new PCData();
		}
		data.setAllowsChildren(true);
		data.setTypeName(EWIAS400DataType.GROUP);
		data.setName(ROOT_NODE);
		data.setOccur(1);
		data.setParameterTypes(true, true);
		// Cell -> Props
		copyNodeData(data, props);

	}

	@Override
	public void saveProperty(NodeCell nodeCell) throws Exception {

		Map<String, Object> props = nodeCell.props;
		EWICell cell = nodeCell.cell;

		// Initial PCData
		PCData data = cell.getPCData();
		if (data == null) {
			data = new PCData();
			cell.setPCData(data);
		}
		data.setAllowsChildren(true);
		data.setTypeName(EWIAS400DataType.GROUP);
		data.setName(ROOT_NODE);
		data.setOccur(1);
		data.setParameterTypes(true, true);
		// Props --> Cell
		copyNodeData(props, data);

		// Update node lable
		updateNodeLabel(cell);

	}

	/**
	 * @param srcData
	 * @param destData
	 */
	private void copyNodeData(PCData srcData, Map<String, Object> destData) {
		String programCallConfigName = srcData.getProgramCallConfigName();
		String programCallHostIP = srcData.getProgramCallHostIP();
		String programCallProgramPath = srcData.getProgramCallProgramPath();
		String key = makeKey(null, "$");
		destData.put("key", key);
		if (programCallConfigName != null) {
			destData.put("programCallConfigName", programCallConfigName);
			destData.put("programCallHostIP", programCallHostIP);
			destData.put("programCallProgramPath", programCallProgramPath);
		}

		int count = srcData.getChildCount();
		if (count > 0) {
			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			destData.put("fields", list);
			for (int i = 0; i < count; i++) {
				PCData node = (PCData) srcData.getChildAt(i);
				Map<String, Object> child = new LinkedHashMap<String, Object>();
				copyData(node, child, makeKey(key, i));
				list.add(child);
			}
		}

	}

	/**
	 * @param srcData
	 * @param destData
	 * @param parentKey
	 */
	private void copyData(PCData srcData, Map<String, Object> destData, String parentKey) {

		String typeName = srcData.getTypeName();
		String timeFormat = srcData.getTimeFormat();
		int numDigits = srcData.getNumDigits();
		int numDecimalPositions = srcData.getNumDecimalPositions();
		int textLength = srcData.getTextLength();
		String arrayTypeName = srcData.getArrayTypeName();
		int arrayLength = srcData.getArrayLength();
		int offset = srcData.getOffset();
		String name = srcData.getName();
		String value = srcData.getValue();
		int occur = srcData.getOccur();
		int outOccur = srcData.getOutOccur();
		boolean inputParameter = srcData.isInputParameter();
		boolean outputParameter = srcData.isOutputParameter();
		boolean collection = srcData.isCollection();
		boolean redefined = srcData.isRedefined();
		boolean replaceInputByRedefined = srcData.canReplaceInputByRedefined();
		int redefinedIndex = srcData.getRedefinedIndex();
		int redefinedOffset = srcData.getRedefinedOffset();

		String fieldName = srcData.getFieldName();
		String fieldNameInput = srcData.getFieldNameInput();
		String fieldNameOutput = srcData.getFieldNameOutput();

		boolean screenFieldInput = srcData.isScreenFieldInput();
		boolean screenFieldOutput = srcData.isScreenFieldOutput();

		boolean promoteOutput = srcData.isPromoteInput();
		boolean promoteInput = srcData.isPromoteOutput();

		String key = makeKey(parentKey, name);

		// Field name
		if (fieldName == null) {
			fieldName = fieldNameInput;
		}
		if (fieldName == null) {
			fieldName = fieldNameOutput;
		}
		if (name == null) {
			name = fieldName;
		}

		destData.put("typeName", typeName);
		destData.put("timeFormat", timeFormat);
		destData.put("numDigits", numDigits);
		destData.put("numDecimalPositions", numDecimalPositions);
		destData.put("textLength", textLength);
		destData.put("arrayTypeName", arrayTypeName);
		destData.put("arrayLength", arrayLength);
		destData.put("offset", offset);
		destData.put("name", name);
		destData.put("value", value);
		destData.put("occur", occur);
		destData.put("outOccur", outOccur);
		destData.put("inputParameter", inputParameter);
		destData.put("outputParameter", outputParameter);
		destData.put("collection", collection);
		destData.put("redefined", redefined);
		destData.put("replaceInputByRedefined", replaceInputByRedefined);
		destData.put("redefinedIndex", redefinedIndex);
		destData.put("redefinedOffset", redefinedOffset);
		destData.put("fieldName", fieldName);
		destData.put("fieldNameInput", fieldNameInput);
		destData.put("fieldNameOutput", fieldNameOutput);
		destData.put("screenFieldInput", screenFieldInput);
		destData.put("screenFieldOutput", screenFieldOutput);
		destData.put("promoteOutput", promoteOutput);
		destData.put("promoteInput", promoteInput);

		destData.put("key", key);
		// set index and offset
		// Calculating index and offset of PCData
		setIndexAndOffset(srcData, destData);

		int count = srcData.getChildCount();
		if (count > 0) {
			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			destData.put("children", list);
			for (int i = 0; i < count; i++) {
				PCData node = (PCData) srcData.getChildAt(i);
				Map<String, Object> child = new LinkedHashMap<String, Object>();
				copyData(node, child, makeKey(key, i));
				list.add(child);
			}
		}

	}

	/**
	 * @param srcData
	 * @param destData
	 */
	@SuppressWarnings("unchecked")
	private void copyNodeData(Map<String, Object> srcData, PCData destData) {
		String programCallConfigName = (String) srcData.get("programCallConfigName");
		String programCallHostIP = (String) srcData.get("programCallHostIP");
		String programCallProgramPath = (String) srcData.get("programCallProgramPath");
		destData.setProgramCallConfigName(programCallConfigName);
		destData.setProgramCallHostIP(programCallHostIP);
		destData.setProgramCallProgramPath(programCallProgramPath);
		List<Map<String, Object>> list = (List<Map<String, Object>>) srcData.get("fields");
		if (list != null) {
			for (int i = 0; i < list.size(); i++) {
				Map<String, Object> child = list.get(i);
				PCData node = new PCData();
				copyData(child, node);
				destData.add(node);
			}
		}
	}

	/**
	 * @param srcData
	 * @param destData
	 */
	@SuppressWarnings("unchecked")
	private void copyData(Map<String, Object> srcData, PCData destData) {
		String fieldName = (String) srcData.get("fieldName");
		String fieldNameInput = (String) srcData.get("fieldNameInput");
		String fieldNameOutput = (String) srcData.get("fieldNameOutput");

		boolean isScreenFieldInput = (boolean) srcData.get("screenFieldInput");
		boolean isScreenFieldOutput = (boolean) srcData.get("screenFieldOutput");

		boolean promoteOutput = (boolean) srcData.get("promoteOutput");
		boolean promoteInput = (boolean) srcData.get("promoteInput");

		String typeName = (String) srcData.get("typeName");
		String timeFormat = (String) srcData.get("timeFormat");
		int numDigits = (int) srcData.get("numDigits");
		int numDecimalPositions = (int) srcData.get("numDecimalPositions");
		int textLength = (int) srcData.get("textLength");
		String arrayTypeName = (String) srcData.get("arrayTypeName");
		int arrayLength = (int) srcData.get("arrayLength");
		int offset = (int) srcData.get("offset");
		String name = (String) srcData.get("name");
		String value = (String) srcData.get("value");
		int occur = (int) srcData.get("occur");
		int outOccur = (int) srcData.get("outOccur");
		boolean inputParameter = (boolean) srcData.get("inputParameter");
		boolean outputParameter = (boolean) srcData.get("outputParameter");
		boolean collection = (boolean) srcData.get("collection");
		boolean redefined = (boolean) srcData.get("redefined");
		boolean replaceInputByRedefined = (boolean) srcData.get("replaceInputByRedefined");
		int redefinedIndex = (int) srcData.get("redefinedIndex");
		int redefinedOffset = (int) srcData.get("redefinedOffset");

		if (fieldName != null) {
			destData.setFieldName(fieldName);
		}
		destData.setFieldNameInput(fieldNameInput);
		destData.setFieldNameOutput(fieldNameOutput);
		destData.setScreenFieldInput(isScreenFieldInput);
		destData.setScreenFieldOutput(isScreenFieldOutput);
		destData.setPromoteInput(promoteOutput);
		destData.setPromoteOutput(promoteInput);

		destData.setTypeName(typeName);
		destData.setTimeFormat(timeFormat);
		destData.setNumDigits(numDigits);
		destData.setNumDecimalPositions(numDecimalPositions);
		destData.setTextLength(textLength);
		destData.setArrayTypeName(arrayTypeName);
		destData.setArrayLength(arrayLength);
		destData.setOffset(offset);
		destData.setName(name);
		destData.setValue(value);
		destData.setOccur(occur);
		destData.setOutOccur(outOccur);
		destData.setParameterTypes(inputParameter, outputParameter);
		destData.setCollection(collection);
		destData.setRedefined(redefined);
		destData.setReplaceInputByRedefined(replaceInputByRedefined);
		destData.setRedefinedIndex(redefinedIndex);
		destData.setRedefinedOffset(redefinedOffset);

		// Reset redefined
		if (!redefined) {
			destData.setRedefinedIndex(0);
			destData.setRedefinedOffset(0);
		}

		List<Map<String, Object>> list = (List<Map<String, Object>>) srcData.get("children");
		if (list != null) {
			for (int i = 0; i < list.size(); i++) {
				Map<String, Object> child = list.get(i);
				PCData node = new PCData();
				copyData(child, node);
				destData.add(node);
			}
		}

	}

	/**
	 * Make redefined data
	 * 
	 * @param node
	 * @param props
	 */
	private void setIndexAndOffset(PCData node, Map<String, Object> props) {
		if (node != null) {
			if (node.isRedefined()) {
				int offset = node.getRedefinedOffset();
				int index = node.getRedefinedIndex();
				props.put("redefinedOffset", offset);
				props.put("redefinedIndex", index);
			} else {
				PCData redefinedParent = node.getRedefinedToParentNode(false);
				int offset = 0;
				int index = 0;
				if (redefinedParent != null) {
					offset = node.getOffsetOfNode(node, redefinedParent) + redefinedParent.getRedefinedOffset();
					index = node.getIndexOfNode(node, redefinedParent) + redefinedParent.getRedefinedIndex();
				} else {
					offset = node.getOffsetOfNode(node, (PCData) node.getRoot());
					index = node.getIndexOfNode(node, (PCData) node.getRoot());
				}
				props.put("redefinedOffset", offset);
				props.put("redefinedIndex", index);
			}

		}
	}

	/**
	 * Merge data from copybook and frontend data
	 * 
	 * @param nodeCell
	 * @param mapObj
	 * @return mapData
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object mergePlugConfig(NodeCell nodeCell, Object mapObj) throws Exception {

		// Old data in property
		Map<String, Object> oldNodeProps = (Map<String, Object>) mapObj;
		String programCallConfigName = (String) oldNodeProps.get("programCallConfigName");

		// Data
		EWICell cell = nodeCell.cell;

		// Call to refresh input/output
		common.ssquare.programCall.refreshInputOutputDataFromCopybook(cell, programCallConfigName);

		// load property
		loadProperty(nodeCell);

		Map<String, Object> newNodeProps = nodeCell.props;

		// Set value from oldNodeProps
		newNodeProps.put("programCallConfigName", oldNodeProps.get("programCallConfigName"));

		// Prepare data to merge
		doMergeMap(oldNodeProps, newNodeProps);

		return newNodeProps;
	}

	/**
	 * Merge new map with old map
	 * 
	 * @param oldMap
	 * @param newMap
	 */
	private void doMergeMap(Map<String, Object> oldMap, Map<String, Object> newMap) {
		// Call Merge Data
		// Input / Output
		// If path is corrected at newNodeProps, set value from oldNodeProps
		if (oldMap != null && newMap != null) {
			copyFieldData(oldMap, newMap);
		}
	}

	/**
	 * To load plugin data to refresh length of copybook
	 */
	@Override
	public Object loadPlugConfig(NodeCell nodeCell) throws Exception {

		// Call save property
		saveProperty(nodeCell);

		// Load data from EWICell
		loadProperty(nodeCell);

		// result
		Map<String, Object> props = nodeCell.props;

		return props;
	}

	/**
	 * copy field
	 * 
	 * @param srcData
	 * @param destData
	 */
	@SuppressWarnings("unchecked")
	private void copyFieldData(Map<String, Object> srcData, Map<String, Object> destData) {
		String srcName = (String) srcData.get("name");
		String destName = (String) destData.get("name");
		if (srcName != null && destName != null && destName.equalsIgnoreCase(srcName)) {

			// *********
			// Data
			// *********
			String fieldNameInput = (String) srcData.get("fieldNameInput");
			String fieldNameOutput = (String) srcData.get("fieldNameOutput");

			boolean screenFieldInput = (boolean) srcData.get("screenFieldInput");
			boolean screenFieldOutput = (boolean) srcData.get("screenFieldOutput");

			boolean promoteOutput = (boolean) srcData.get("promoteOutput");
			boolean promoteInput = (boolean) srcData.get("promoteInput");

			destData.put("fieldNameInput", fieldNameInput);
			destData.put("fieldNameOutput", fieldNameOutput);
			destData.put("screenFieldInput", screenFieldInput);
			destData.put("screenFieldOutput", screenFieldOutput);
			destData.put("promoteOutput", promoteOutput);
			destData.put("promoteInput", promoteInput);
			// *********

			List<Map<String, Object>> srcList = (List<Map<String, Object>>) srcData.get("children");
			List<Map<String, Object>> destList = (List<Map<String, Object>>) destData.get("children");
			if (srcList != null && destList != null) {
				for (int i = 0; i < destList.size(); i++) {
					Map<String, Object> child = destList.get(i);
					String name = (String) child.get("name");
					if (name != null) {
						for (int j = 0; j < srcList.size(); j++) {
							Map<String, Object> tmp = srcList.get(j);
							if (name.equalsIgnoreCase((String) tmp.get("name"))) {
								copyFieldData(tmp, child);
								break;
							}
						}
					}
				}
			}

		}
	}

	@Override
	public Map<String, Object> listDataListName(NodeCell nodeCell) {

		// Property from font-end
		Map<String, Object> props = nodeCell.props;

		// Find list name
		Map<String, Object> data = findField(props, "fields", "name", "Map", "collection", true, true, true);

		return data;
	}

	@Override
	public Map<String, Object> listFieldName(NodeCell nodeCell) {

		// Property from font-end
		Map<String, Object> props = nodeCell.props;

		// Find list name
		Map<String, Object> data = findField(props, "fields", "name", "Map", "collection", false, true, true);

		return data;
	}

	@Override
	public Map<String, Object> listFieldNameInDataList(NodeCell nodeCell, String listName) {

		// Property from font-end
		Map<String, Object> props = nodeCell.props;

		// Find list name
		Map<String, Object> data = findFieldInList(props, "fields", "name", "Map", "collection", listName, true, true);

		return data;
	}

}
