package com.ssquare.util;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ssquare.facade.common.Common;
import com.ssquare.facade.common.config.PropConfig;

public class WidgetsUtil extends Common {

	// widget data
	private static Map<String, Object> widgets = null;

	/** don't create new instance */
	private WidgetsUtil() {
	}

	/**
	 * Get widget object
	 * 
	 * @return
	 * @throws IOException
	 */
	public static Map<String, Object> getWidgets() throws IOException {

		if (widgets == null) {

			// Get default props
			PropConfig props = common.getProp();
			String allNodePath = props.getValue("path.ssquare.allNodes");

			// Read widget from template file
			File templateFile = new File(allNodePath);
			widgets = common.json.toMap(templateFile);

		}

		return widgets;
	}

	/**
	 * Get widget by type
	 * 
	 * @param nodeType
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, Object> getWidgetByType(String nodeType) {
		try {
			
			Map<String, Object> widgets = getWidgets();
			
			if(nodeType.equalsIgnoreCase("MULTIDECISION")) {
				nodeType = "DECISION";
			}

			if (widgets != null && widgets.get("widgets") != null) {
				List<Map<String, Object>> list = (List<Map<String, Object>>) widgets.get("widgets");
				for (Map<String, Object> widget : list) {
					String wNodeType = (String) widget.get("nodeType");
					
					if (wNodeType.equals(nodeType)) {
						return widget;
					}
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Get node name by type
	 * @param nodeType
	 * @return
	 */
	public static String getNodeNameByType(String nodeType) {
		Map<String, Object> widget = getWidgetByType(nodeType);
		if(widget != null) {
			String nodeName = (String) widget.get("nodeName");
			return nodeName;
		}
		return null;
	}

	/**
	 * Get panel info from config
	 * 
	 * @param nodeType
	 * @return
	 */
	public static Map<String, Object> getPanelInfo(String nodeType) {

		Map<String, Object> widget = getWidgetByType(nodeType);

		if (widget != null) {

			Map<String, Object> map = new HashMap<String, Object>();
			map.put("panelWidth", widget.get("panelWidth"));
			map.put("icon", widget.get("icon"));
			//map.put("width", widget.get("width"));
			//map.put("height", widget.get("height"));
			map.put("system", "SSQUARE");
			map.put("title", widget.get("buttonName"));

			return map;
		}

		return null;

	}

	/**
	 * Get node infomation tab
	 * 
	 * @param nodeType
	 * @return
	 */
	public static Map<String, Object> getNodeInfo(String nodeType) {

		// TODO
		// EDIT Node Info in the future
		Map<String, Object> nodeInfo = new HashMap<String, Object>();

		String info = "<div><b> The infomation will be success in the future, please wait... </b></div>";

		nodeInfo.put("type", "html");
		nodeInfo.put("info", info);

		return nodeInfo;
	}

}
