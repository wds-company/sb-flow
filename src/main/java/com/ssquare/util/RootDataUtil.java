package com.ssquare.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.ssquare.consts.DataTypeConsts;

public class RootDataUtil {

	private static SortPromoteData sorter = new SortPromoteData();

	/**
	 * Get Root of SSQ data
	 * 
	 * @param oldRootData
	 * @param contentData
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<Map<String, Object>> getRootSSQData(List<Map<String, Object>> oldRootData, List<Map<String, Object>> contentData) {
		List<Map<String, Object>> root = oldRootData;
		if (oldRootData == null) {
			root = new ArrayList<Map<String, Object>>();
		}

		String defaultValue = "";

		Map<String, Object> content = getInList(root, "content");
		if (content == null) {
			content = makeField("content", "ROOT.content", DataTypeConsts.TYPE_MAP, DataTypeConsts.CREATE_TYPE_RESERVE, defaultValue, true, true);
			root.add(content);
		}

		List<Map<String, Object>> children = (List<Map<String, Object>>) content.get("children");
		if (children == null) {
			children = new ArrayList<Map<String, Object>>();
		}

		// loop to add value
		if (contentData != null) {
			for (Map<String, Object> map : contentData) {
				String name = (String) map.get("name");
				Map<String, Object> value = getInList(children, name);
				if (value == null) {
					children.add(map);
				} else {
					Object ch = map.get("children");
					value.put("children", ch);
				}
			}
		}

		// Add reserved field
		addReservedField(root);

		return root;
	}

	/**
	 * Get field in list by name
	 * 
	 * @param list
	 * @param name
	 * @return
	 */
	public static Map<String, Object> getInList(List<Map<String, Object>> list, String name) {

		if (list != null) {
			for (Map<String, Object> map : list) {
				String mname = (String) map.get("name");
				if (mname.equals(name)) {
					return map;
				}
			}
		}

		return null;
	}

	/**
	 * Add map to list
	 * 
	 * @param list
	 * @param map
	 */
	public static void addInListIfNotExist(List<Map<String, Object>> list, Map<String, Object> map) {
		String name = (String) map.get("name");
		Map<String, Object> smap = getInList(list, name);
		if (smap == null) {
			list.add(map);
		} else {
			Object ch = map.get("children");
			smap.put("children", ch);
		}
	}

	/**
	 * add reserved field
	 * 
	 * @param root
	 */
	@SuppressWarnings("unchecked")
	private static void addReservedField(List<Map<String, Object>> root) {

		Map<String, Object> version = makeField("version", "ROOT.version", DataTypeConsts.TYPE_STRING, DataTypeConsts.CREATE_TYPE_RESERVE, "", true, true);
		addInListIfNotExist(root, version);

		Map<String, Object> ewitoken = makeField("ewitoken", "ROOT.ewitoken", DataTypeConsts.TYPE_STRING, DataTypeConsts.CREATE_TYPE_RESERVE, "", true, true);
		addInListIfNotExist(root, ewitoken);

		Map<String, Object> usergroup = makeField("usergroup", "ROOT.usergroup", DataTypeConsts.TYPE_STRING, DataTypeConsts.CREATE_TYPE_RESERVE, "", true, false);
		addInListIfNotExist(root, usergroup);

		Map<String, Object> Username = makeField("Username", "ROOT.Username", DataTypeConsts.TYPE_STRING, DataTypeConsts.CREATE_TYPE_RESERVE, "", true, false);
		addInListIfNotExist(root, Username);

		Map<String, Object> workstation = makeField("workstation", "ROOT.workstation", DataTypeConsts.TYPE_STRING, DataTypeConsts.CREATE_TYPE_RESERVE, "", true, false);
		addInListIfNotExist(root, workstation);

		Map<String, Object> Returncode = makeField("Returncode", "ROOT.Returncode", DataTypeConsts.TYPE_STRING, DataTypeConsts.CREATE_TYPE_RESERVE, "", false, true);
		addInListIfNotExist(root, Returncode);

		Map<String, Object> Runmessage = makeField("Runmessage", "ROOT.Runmessage", DataTypeConsts.TYPE_STRING, DataTypeConsts.CREATE_TYPE_RESERVE, "", false, true);
		addInListIfNotExist(root, Runmessage);

		Map<String, Object> Returnscreen = makeField("Returnscreen", "ROOT.Returnscreen", DataTypeConsts.TYPE_STRING, DataTypeConsts.CREATE_TYPE_RESERVE, "", false, true);
		addInListIfNotExist(root, Returnscreen);

		Map<String, Object> content = getInList(root, "content");
		if (content != null) {
			List<Map<String, Object>> children = (List<Map<String, Object>>) content.get("children");
			if (children == null) {
				children = new ArrayList<Map<String, Object>>();
				content.put("content", children);
			}

			Map<String, Object> username = makeField("username", "ROOT.content.username", DataTypeConsts.TYPE_STRING, DataTypeConsts.CREATE_TYPE_RESERVE, "", true, false);
			addInListIfNotExist(children, username);

			Map<String, Object> password = makeField("password", "ROOT.content.password", DataTypeConsts.TYPE_STRING, DataTypeConsts.CREATE_TYPE_RESERVE, "", true, false);
			addInListIfNotExist(children, password);
		}

	}

	/**
	 * Make field
	 * 
	 * @param name
	 * @param path
	 * @param fieldType
	 * @param createType
	 * @param promoteIn
	 * @param promoteOut
	 * @param forDropdown
	 * @return
	 */
	public static final Map<String, Object> makeField(String name, String path, String fieldType, String createType, String defaultValue, boolean promoteIn, boolean promoteOut) {

		fieldType = getTypeName(fieldType, false);
		createType = getTypeName(createType, false);

		HashMap<String, Object> field = new HashMap<String, Object>();

		field.put("label", name);
		field.put("value", name);

		field.put("key", UUID.randomUUID().toString());
		field.put("path", path);
		field.put("promote_input", promoteIn);
		field.put("promote_output", promoteOut);
		field.put("create_type", createType);
		field.put("default_value", defaultValue);

		field.put("name", name);
		field.put("fieldType", fieldType);

		if (fieldType.equalsIgnoreCase(DataTypeConsts.TYPE_ARRAY) || fieldType.equalsIgnoreCase(DataTypeConsts.TYPE_MAP)) {
			field.put("children", new ArrayList<Map<String, Object>>());
		}

		return field;
	}

	/**
	 * Update value in field
	 * 
	 * @param name
	 * @param path
	 * @param fieldType
	 * @param createType
	 * @param promoteIn
	 * @param promoteOut
	 * @param forDropdown
	 * @return
	 */
	public static final Map<String, Object> updateField(Map<String, Object> field, String name, String path, String fieldType, String createType, String defaultValue, boolean promoteIn, boolean promoteOut) {

		fieldType = getTypeName(fieldType, false);
		createType = getTypeName(createType, false);

		field.put("label", name);
		field.put("value", name);

		field.put("key", UUID.randomUUID().toString());
		field.put("path", path);
		field.put("promote_input", promoteIn);
		field.put("promote_output", promoteOut);
		field.put("create_type", createType);
		field.put("default_value", defaultValue);

		field.put("name", name);
		field.put("fieldType", fieldType);

		if (fieldType.equalsIgnoreCase(DataTypeConsts.TYPE_ARRAY) || fieldType.equalsIgnoreCase(DataTypeConsts.TYPE_MAP)) {
			if (field.get("children") == null) {
				field.put("children", new ArrayList<Map<String, Object>>());
			}
		}

		return field;
	}

	/**
	 * Convert field from promote field to ssq promote field
	 * 
	 * @param field
	 * @return
	 */
	public static final Map<String, Object> convertSsqField(Map<String, Object> field, String path) {

		// String name = (String) field.get("name");
		String fieldType = (String) field.get("fieldType");
		boolean editor = false;
		Boolean promote_input = (Boolean) field.get("promote_input");
		Boolean promote_output = (Boolean) field.get("promote_output");
		String create_type = (String) field.get("create_type");
		String default_value = (String) field.get("default_value");

		HashMap<String, Object> ssqField = new HashMap<String, Object>();

		ssqField.put("path", path);
		ssqField.put("promote_input", promote_input);
		ssqField.put("promote_output", promote_output);
		ssqField.put("create_type", create_type.toUpperCase());
		ssqField.put("default_value", default_value);
		ssqField.put("editor", editor);
		ssqField.put("type", fieldType.toUpperCase());

		return ssqField;
	}

	public static final String getTypeName(String type, boolean isUpperCase) {
		if (isUpperCase) {
			return type.toUpperCase();
		} else {
			type = type.toLowerCase();
			Character c = type.charAt(0);
			c = Character.toUpperCase(c);
			String nType = c.toString();
			if (type.length() > 1) {
				nType = nType + type.substring(1, type.length());
			}
			return nType;
		}
	}

	/**
	 * Merge promote data
	 * 
	 * @param newPromote
	 * @param oldPromote
	 * @return
	 */
	public static final List<Map<String, Object>> mergePromoteField(List<Map<String, Object>> newPromote, List<Map<String, Object>> oldPromote) {

		// Finding Promote Flag
		setPromoteFlag(newPromote, oldPromote);

		// Copy manual field
		copyManualFieldsFromOld(newPromote, oldPromote);

		return newPromote;
	}

	/**
	 * To set promote flag from oldList to newList
	 * 
	 * @param newList
	 * @param oldList
	 */
	@SuppressWarnings("unchecked")
	private static final void setPromoteFlag(List<Map<String, Object>> newList, List<Map<String, Object>> oldList) {

		for (Map<String, Object> newMap : newList) {

			String name = (String) newMap.get("name");
			Map<String, Object> oldMap = getInList(oldList, name);

			copyPromoteFlag(newMap, oldMap);

			List<Map<String, Object>> newChildren = (List<Map<String, Object>>) newMap.get("children");
			List<Map<String, Object>> oldChildren = null;
			if (oldMap != null) {
				oldChildren = (List<Map<String, Object>>) oldMap.get("children");
			}

			if (newChildren != null && !newChildren.isEmpty()) {
				setPromoteFlag(newChildren, oldChildren);
			}
		}

	}

	/**
	 * Copy promote flage from old to new
	 * 
	 * @param newMap
	 * @param oldMap
	 */
	private static final void copyPromoteFlag(Map<String, Object> newMap, Map<String, Object> oldMap) {
		if (oldMap != null) {
			newMap.put("promote_input", oldMap.get("promote_input"));
			newMap.put("promote_output", oldMap.get("promote_output"));
		}

		if (newMap.get("promote_input") == null) {
			newMap.put("promote_input", true);
		}

		if (newMap.get("promote_output") == null) {
			newMap.put("promote_output", true);
		}

	}

	/**
	 * Copy manual data to new list
	 * 
	 * @param newList
	 * @param oldList
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private static final boolean copyManualFieldsFromOld(List<Map<String, Object>> newList, List<Map<String, Object>> oldList) {

		boolean hasManual = false;
		if (oldList == null || oldList.isEmpty()) {
			return hasManual;
		}

		// Loop to merge
		for (Map<String, Object> oldMap : oldList) {

			// Old data
			String name = (String) oldMap.get("name");
			String create_type = (String) oldMap.get("create_type");
			String path = (String) oldMap.get("key");
			Boolean promote_input = (Boolean) oldMap.get("promote_input");
			Boolean promote_output = (Boolean) oldMap.get("promote_output");
			String default_value = (String) oldMap.get("default_value");
			String fieldType = (String) oldMap.get("fieldType");

			Map<String, Object> newMap = getInList(newList, name);
			;

			// Find data as manual
			if (DataTypeConsts.CREATE_TYPE_MANUAL.equalsIgnoreCase(create_type)) {
				if (newMap == null) {

					// make new map
					newMap = makeField(name, path, fieldType, create_type, default_value, promote_input, promote_output);

					// add to list
					if (newList != null) {
						newList.add(newMap);
					}

				} else {

					// update field
					updateField(newMap, name, path, fieldType, create_type, default_value, promote_input, promote_output);
				}

				hasManual = true;
			}

			List<Map<String, Object>> oldChildren = (List<Map<String, Object>>) oldMap.get("children");
			List<Map<String, Object>> newChildren = null;
			if (newMap != null) {
				newChildren = (List<Map<String, Object>>) newMap.get("children");
			}

			// Make new children
			if (newChildren == null) {
				newChildren = new ArrayList<Map<String, Object>>();
			}

			boolean hasSubManual = false;
			if (oldChildren != null && !oldChildren.isEmpty()) {
				hasSubManual = copyManualFieldsFromOld(newChildren, oldChildren);
			}

			if (hasSubManual) {

				// Create parent of children
				newMap = getInList(newList, name);
				if (newMap == null) {
					// make new map
					newMap = makeField(name, path, fieldType, create_type, default_value, promote_input, promote_output);
					// add to list
					if (newList != null) {
						newList.add(newMap);
					}
				} else {

					// update field
					updateField(newMap, name, path, fieldType, create_type, default_value, promote_input, promote_output);
				}

				// put data back to children
				newMap.put("children", newChildren);

			}

			hasManual = hasSubManual;

		}

		return hasManual;

	}

	@SuppressWarnings("unchecked")
	public static List<Map<String, Object>> sortPromoteField(List<Map<String, Object>> newPropModel) {

		// Sort sub field
		for (Map<String, Object> map : newPropModel) {
			List<Map<String, Object>> children = (List<Map<String, Object>>) map.get("children");
			if (children != null && !children.isEmpty()) {
				sortPromoteField(children);
			}
		}

		Collections.sort(newPropModel, sorter);

		return newPropModel;
	}

	/**
	 * Show type in propModel
	 * 
	 * @param newPropModel
	 * @param showType
	 */
	@SuppressWarnings("unchecked")
	public static void splitPromote(List<Map<String, Object>> newPropModel, List<Map<String, Object>> buffer, boolean showType, boolean splitInput) {
		if (newPropModel != null) {
			for (Map<String, Object> map : newPropModel) {

				String name = (String) map.get("name");
				String key = (String) map.get("key");
				String fieldType = (String) map.get("fieldType");
				Boolean promote_input = (Boolean) map.get("promote_input");
				Boolean promote_output = (Boolean) map.get("promote_output");

				Map<String, Object> field = makdSplitField(name, fieldType, key, showType);

				List<Map<String, Object>> children = (List<Map<String, Object>>) map.get("children");

				if (splitInput && promote_input) {

					buffer.add(field);

					if (children != null) {
						List<Map<String, Object>> buffChildren = new ArrayList<Map<String, Object>>();
						field.put("children", buffChildren);
						splitPromote(children, buffChildren, showType, splitInput);
					}

				} else if (!splitInput && promote_output) {

					buffer.add(field);

					if (children != null) {
						List<Map<String, Object>> buffChildren = new ArrayList<Map<String, Object>>();
						field.put("children", buffChildren);
						splitPromote(children, buffChildren, showType, splitInput);
					}
				}

			}
		}
	}

	/**
	 * Make simple field
	 * 
	 * @param name
	 * @param type
	 * @param showType
	 * @return
	 */
	public static Map<String, Object> makdSplitField(String name, String type, String key, boolean showType) {
		Map<String, Object> map = new HashMap<String, Object>();

		String title = name;
		if (showType) {
			title = name + " : " + type;
		}

		map.put("title", title);
		map.put("key", key);

		return map;
	}

	/**
	 * List field in promote data
	 * 
	 * @param promote
	 * @param buffer
	 * @return list
	 */
	@SuppressWarnings("unchecked")
	public static List<Map<String, Object>> listFieldNameInPromote(List<Map<String, Object>> promote, List<Map<String, Object>> buffer, boolean isDataList) {
		if (buffer == null) {
			buffer = new ArrayList<Map<String, Object>>();
		}

		if (promote != null) {
			for (Map<String, Object> map : promote) {
				String name = (String) map.get("name");
				String fieldType = (String) map.get("fieldType");
				String createType = (String) map.get("create_type");
				
				if(map.get("label") == null) {
					map.put("label", name);
				}
				if(map.get("value") == null) {
					map.put("value", name);
				}
				
				if (isDataList) {
					if (DataTypeConsts.TYPE_ARRAY.equals(fieldType) && DataTypeConsts.CREATE_TYPE_MANUAL.equals(createType)) {
						if (getInList(buffer, name) == null) {							
							buffer.add(map);
						}
					}
				} else {
					if (DataTypeConsts.CREATE_TYPE_MANUAL.equals(createType)) {
						if (!DataTypeConsts.TYPE_ARRAY.equals(fieldType) || !DataTypeConsts.TYPE_MAP.equals(fieldType)) {
							if (getInList(buffer, name) == null) {
								buffer.add(map);
							}
						}
					}
				}

				List<Map<String, Object>> children = (List<Map<String, Object>>) map.get("children");
				if (children != null && !children.isEmpty()) {
					if (isDataList) {
						buffer = listFieldNameInPromote(children, buffer, isDataList);
					} else {
						if (!DataTypeConsts.TYPE_ARRAY.equals(fieldType)) {
							buffer = listFieldNameInPromote(children, buffer, isDataList);
						}else {
							// break recursive
						}
					}
				}
			}
		}

		return buffer;
	}

}

/**
 * Comparator class
 * 
 * @author adirak-vdi
 *
 */
class SortPromoteData implements Comparator<Map<String, Object>> {
	@Override
	public int compare(Map<String, Object> o1, Map<String, Object> o2) {

		String n1 = (String) o1.get("name");
		String n2 = (String) o2.get("name");

		String ct1 = (String) o1.get("create_type");
		String ct2 = (String) o2.get("create_type");

		if ("content".equals(n1) && DataTypeConsts.CREATE_TYPE_RESERVE.equals(ct1)) {
			n1 = "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzz";
		} else if (DataTypeConsts.CREATE_TYPE_RESERVE.equals(ct1)) {
			n1 = "00_" + n1;
		}

		if ("content".equals(n2) && DataTypeConsts.CREATE_TYPE_RESERVE.equals(ct2)) {
			n2 = "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzz";
		} else if (DataTypeConsts.CREATE_TYPE_RESERVE.equals(ct2)) {
			n2 = "00_" + n2;
		}

		n1 = n1.toLowerCase();
		n2 = n2.toLowerCase();

		return n1.compareTo(n2);
	}
}
