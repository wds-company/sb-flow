package com.ssquare.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MapUtil {

	/**
	 * Put all and merge field
	 * 
	 * @param data
	 * @param subData
	 */
	@SuppressWarnings("unchecked")
	public static final void putAllFieldToMap(Map<String, Object> data, Map<String, Object> subData) {
		if (data != null && subData != null) {
			Iterator<String> iter = subData.keySet().iterator();
			while (iter.hasNext()) {

				String key = iter.next();
				Map<String, Object> map = (Map<String, Object>) subData.get(key);
				List<Map<String, Object>> children = (List<Map<String, Object>>) map.get("children");

				// check children
				if (children != null && !children.isEmpty()) {

					Map<String, Object> mmap = (Map<String, Object>) data.get(key);
					if (mmap == null) {
						data.put(key, map);
					} else {
						
						List<Map<String, Object>> mchildren = (List<Map<String, Object>>) mmap.get("children");
						if (mchildren != null) {
							int size = children.size();
							for (int i = 0; i < size; i++) {
								Map<String, Object> sMap = children.get(i);
								if (!isContains(mchildren, sMap)) {
									mchildren.add(sMap);
								}
							}
						} else {
							data.put(key, map);
						}
						
					}

				} else {
					data.put(key, map);
				}
			}
		}
	}

	public static final boolean isContains(List<Map<String, Object>> list, Map<String, Object> map) {
		String objName = (String) map.get("name");
		if (objName != null) {
			for (Map<String, Object> mmap : list) {
				String name = (String) mmap.get("name");
				if (objName.equals(name)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Convert map to array and sort by name
	 * 
	 * @param buffer
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static final List<Map<String, Object>> mapToArray(Map<String, Object> buffer) {

		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

		Iterator<String> iter = buffer.keySet().iterator();
		while (iter.hasNext()) {
			String key = iter.next();
			Map<String, Object> map = (Map<String, Object>) buffer.get(key);
			list.add(map);
		}

		Comparator<Map<String, Object>> comp = new Comparator<Map<String, Object>>() {
			@Override
			public int compare(Map<String, Object> o1, Map<String, Object> o2) {
				String n1 = (String) o1.get("name");
				String n2 = (String) o2.get("name");
				return n1.compareTo(n2);
			}
		};

		Collections.sort(list, comp);

		return list;

	}

}
