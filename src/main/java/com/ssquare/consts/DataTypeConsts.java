package com.ssquare.consts;

public class DataTypeConsts {

	private DataTypeConsts() {
	}

	public static final String TYPE_MAP = "Map";
	public static final String TYPE_ARRAY = "Array";
	public static final String TYPE_STRING = "String";
	public static final String TYPE_INTEGER = "Integer";
	public static final String TYPE_NUMERIC = "Numeric";
	public static final String TYPE_BOOLEAN = "Boolean";
	public static final String TYPE_OBJECT = "Object";

	public static final String CREATE_TYPE_RESERVE = "Reserved";
	public static final String CREATE_TYPE_AUTO = "Auto";
	public static final String CREATE_TYPE_MANUAL = "Manual";

}
